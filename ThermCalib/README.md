# A Hot End thermistor Calibration Tool

![Hardware Photo](thermrig.jpg)

This is calibration tool to help build correct custom look up tables for Reprap style 3D printers. The inputs are a thermocouple temperature sensor and a piggy backed connection to the 3D printers thermistor input. Starting from cold the temperature of the hot end is incremented by a known number of degrees measured by the thermistor the temperature measurement is allowed to stabilise and a record is made of the analogue signal as seen by the 3D printer. once the entire actual operating range of the hot end has been covered the resulting records are used to construct a custom but correct look up table for the hot end under test. If the initial look up table in the printer's firmware is too much out it may be necessary to carry out this process twice. The second time after the new more accurate table has replaced the old inaccurate one. To save time it is often quicker to do the second procedure in reverse starting at the hot ends maximum operating temperature and reducing the temperature a known number of degrees at a time.

The same procedure can also be used to construct a similar look up table for the heated bed thermistor by following the same process. 

This process although a little long winded has the result of producing a guaranteed correct look up table for any thermistor without prior knowledge of it's type or whether it complies with it's manufacturers specification. This results in a more predictable printing and heating temperature.

The same process can be applied to older used thermistors to correct for ageing.

![Screen Grab](therm.jpg)

Default serial parameters for the calibration tool are 19200 8N1, on a linux workstation screen may be used as a simple serial terminal to view the tools output.

`screen /dev/myserialdevice 19200`



