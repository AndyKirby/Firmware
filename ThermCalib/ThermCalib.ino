/*
 Reprap Thermistor claibration aid.
 
 This relies largely on the fact that the ADC section in a Diecimila is the same as the ADC section in a Mega
 so measuring the Thermistor analogue value and comparing it to a reasonably accurate thermometer value
 using the same ADC and vRef alows us to construct a linearised Thermistor table by Empirical Trial and error.
 
 The thermocouple is placed inside the assembled extruder nozzle. To give accurate feedback of the 
 temperatures that the filament will experience.
 
 The Thermistor is embeded into the heater block of the extruder and connected and powered from the RAMPS
 boards. The Diecimila analogue port being used to measure what the thermistor is reading is coupled
 in parrallel across the thermistor.
 
 The Diecimilla then give us two ADC values a linearish Thermoucouple+AD595 derived value and The Thermister
 derived value for the same nozzle temperature.
 
 To use this we set the machine nozzle temperature in increments via G-code (having used some sort of 
 aproximated thermistor table in the fimrware build) the value will be inaccurate but this does'nt matter
 for now. Reading the temperature we want for the Table from the thermistor. When the temperature we want
 is stable we read off the thermistor ADC Value for the table.
 
 By working through the values this way we can construct a reasonably accurate table. That reflects the
 temperature that the filament inside the nozzle will be experiencing.
 
 I have found empiricaly that measuring the temperature on theoutside fo the heater/nozzle is misleading
 and can be out by as much as 50 Deg C. In my case this was enough to actively burn the ABS filament.
 
 It may be out a little but so little it does'nt realy matter. We could oversample and decimate but the 
 precision for this app is not that critical.
 
 This app could also be writen more eficiently, but again in this case itis not ritical and I have gone
 for readability over performance.
*/

  // Global variables, some of which are actualy constants
  char buf[80];        // A working character buffer
  int maxadc = 1024;   // The maximum number of values a 10Bit ADC can represent
  float vref = 5.0;    // The analogue voltage reference used by the ADC
  int bittybit;        // Somewhere to put fractional parts when converting from float to int
  float vdegc = 0.010; // Volts per Degree C from the A595, usualy 0.010 or 10mV
 
  // variables for the metering thermometer
  // AD595 with Thermocouple, 0.010mv per Deg C
  int mtrport = 2;  
  int mtrval = 0;
  float mtrvoltage = 0.0;   
  float mtrdegc = 0.0;
  
  // variables for the target being calibrated
  // Embeded Thermistor in my case
  int tgtport = 4;  
  int tgtval = 0;
  
  // Sample period, in milliseconds
  int samperiod = 5000;
  
  // samples per period
  int avg = 32;

void setup() {                
  // initialize the digital pin as an output.
  // Pin 13 has an LED connected on most Arduino boards:
  pinMode(13, OUTPUT);
  
  Serial.begin(19200);

}

void loop() {

  // read the Thermometer and the Thermistor values
  gettherms();
  
  // Convert the Thermometer value to a voltage and thence to a Temperature
  mtrvoltage = vref / (float)maxadc * (float)mtrval;
  mtrdegc = mtrvoltage / vdegc;  
  
  // convert the fractional part of the temp to an integer
  bittybit = (mtrdegc - (int)mtrdegc) * 100;
 
  // Create our information line for this iteration
  sprintf(buf, "Thermocouple Temp %0d.%d Deg C : Thermistor ADC %d", (int)mtrdegc, bittybit, tgtval);
  
  // Publish it via the serial port
  Serial.println(buf);
  
}

// Should take about a samperiod and performs an average on avg therm readings 
void gettherms(){
  long ad595 = 0;
  long adport = 0;
  int loop, temp595, tempad;
  
  for( loop = 0; loop < avg; loop++ ){
    temp595 = analogRead(mtrport);
    tempad = analogRead(tgtport);
    
    //sprintf(buf, "temp595: %d tmpad: %d", temp595, tempad );
    //Serial.println(buf);    
    
    ad595 += temp595;
    adport += tempad;
    delay((int) samperiod / avg);
  }
  
  mtrval = (int) ad595 / avg;
  tgtval = (int) adport / avg;
  
  //sprintf(buf, "mtrval: %d for %ld, tgtval: %d for %ld", mtrval, ad595, tgtval, adport );
  //Serial.println(buf);
  
   return;
}
  
  
  
  
