#!/usr/bin/perl

#
# Perl script to read temerature and humidity from USB Serial connected 
# sensors and send them to ganglia for graphing
#
use IO::Termios ();

my $USBSerial = '/dev/ttyACM0';
my $SSettings = '115200,8,n,1';
my $comment = '=';
my $Line;
my $device;
my $temperature;
my $humidity;
my $cliopt;

my $handle = IO::Termios->open($USBSerial, $SSettings) or die "IO::Termios->open: $!";

# read the port line-by-line
while ($Line = <$handle>) {
   # if there is a line to process
   if($Line ne "") {
      # and the line is not a comment  
      if($comment ne substr($Line,0,1)) {
         # remove cr/lf at the end of the string
         $Line =~ s/\r?\n//;
         # parse the line into its fields
         ($device, $temperature, $humidity) = split(',', $Line);
         # publish temperature measurement to ganglia
         $cliopt = qq(--name="${device}_temperature" --type="float" --value="${temperature}" --units="DegC" --group="Server_Room" -d 65);
         `/usr/bin/gmetric $cliopt`;
         # publish humidity measurement to ganglia
         $cliopt = qq(--name="${device}_humidity" --type="float" --value="${humidity}" --units="RH%" --group="Server_Room" -d 65);
         `/usr/bin/gmetric $cliopt`;
      }
   }	
}

close $handle;
