/*
  2THMonitor
  Read temperature and humidity from 2 dht22/am2302 and report them to USB

  Intended for monitoring the temperatures and muditiy in a server room both
  inside a server rack and for the room in general.
 
  Tiewrap the controler to somwhere convenitent. 

  Plug the USB cable into a convenient server that is running the 
  ganclia client, create a script to read the sensors via USB and inject the
  measurements into ganglia via Gmetric

  Place one sensor in the top of the rack to be monitored.

  Place the other sensor on the outside of the rack aprox.
  chest height. Usualy best placed on the side of the rack that
  shields the sensor form the in and out air streams for the rack. 



*/
#include <SimpleDHT.h>

#define PERIOD 60000 // read values every PERIOD milliseconds


int pinAM2302a = 6;
int pinAM2302b = 15;
SimpleDHT22 AM2302a(pinAM2302a);
SimpleDHT22 AM2302b(pinAM2302b);

void setup() {
   Serial.begin(115200);
   while (!Serial) {
      ; // wait for serial port to connect. Needed for native USB port only
   }

  // print header
  Serial.println("========================================");
  Serial.println("= device, Temperature *c, Humidity RH% =");
  Serial.println("========================================");
}

void loop() {
   float temperature = 0.0;
   float humidity = 0.0;
   int err = SimpleDHTErrSuccess;
   

   // read AM2302a  
   if((err = AM2302a.read2(&temperature, &humidity, NULL)) != SimpleDHTErrSuccess) {
      Serial.print("Read AM2302a failed, err=");
      Serial.println(err);
      return;
   }  
   Serial.print("AM2302a,");
   Serial.print((float)temperature);
   Serial.print(",");
   Serial.println((float)humidity);

   // read AM2302b
   if((err = AM2302b.read2(&temperature, &humidity, NULL)) != SimpleDHTErrSuccess) {
      Serial.print("Read AM2302b failed, err=");
      Serial.println(err);
      return;
   }
   Serial.print("AM2302b,");
   Serial.print((float)temperature);
   Serial.print(",");
   Serial.println((float)humidity);

   // wait the specified period between readings
   delay(PERIOD);

   return;
}
