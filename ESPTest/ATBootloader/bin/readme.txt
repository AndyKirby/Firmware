Please download these bins to the specified address.

Bin                           Address
boot_v1.1.bin---------------->0x00000
user1.bin-------------------->0x01000    ---> you can use the newest version or a specific version.
esp_init_data_default.bin---->0x7C000
blank.bin-------------------->0x7E000

using esptool

esptool -cp /dev/ttyUSB0 -cb 74880 -ca 0x00000 -cf boot_v1.1.bin -ca 0x01000 -cf user1.bin -ca 0x7C000 -cf esp_init_data_default.bin -ca 0x7E000 -cf blank.bin -vv


