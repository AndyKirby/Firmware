A WiFi sensor cluster for a home automation NoT (Network of things).

The sensor cluster is built as a lamp and functions as an automatic lamp. It is mains powered and monitors the home environment, reporting it's state via MQTT (Mosquitto). Using MQTT as a middle layer seperates out the NoT from the command, control and overview. The firmware is writen to operate autonomously, reporting it's status at regular intervals to MQTT as well as monitor an MQTT topic for instructions that over ride its current mode. For example a light reset instruction will reset the light timer effectively turning off the light until it is triggered again. A light on instruction will turn on the light as if triggered by it's occupation sensor for the default period of time. Light colour command will set the colour of the lamp. etc

This is the firmware for the ESP8266 part of the lamp.

The firmware is a work in progress and starts simple as a reporting automatic lamp that also monitors light level, room occupation, temperature and humidity. Thereafter features will be added on an adhoc basis until there are no longer any resources available (Pins, RAM, Processor Cycles)

Wish List of extras:-

* Automatic soft lighting during night time operation
* Flickering colour light to simulate a TV within the room
* Electronics, https://github.com/AndyKirby/Electronics/tree/master/NoTLamp
* Carbon dioxide measurement
* Smoke detector
* Measure levels of light types (IR, UV Visible)
* Sound level measuring
* Vibration measuring (Earthquake alarm, intrusion detection)
* Gas and combustible gasses monitoring
* Radon and background radiation monitoring

Whilst the design is autonomous and can be used without control and supervision, the addition of this and the meta data it can infer, provides additional benefits:-

* Intrusion detection, lamps signaling occupancy whist house is empty
* Fire detection through rate of temperature rise and maximum temperature thresholds
* Timed lighting to simulate house occupancy whilst away
* Flickering coloured light to simulate TV useage whilst away
* Alarm notification, lighting up the house when intrusion is detected
* Signaling notifiable events through coloured lamp flashes
* Light based alarm clock for waking up rising evidenced by ocupation sensing from other lamps
* Central Heating control based on room, zone and whole house temperatures
* Warn of condensation and mold/mildew potential
* Warn of irritable environmnet as too dry
* Create room, zone and whole house figures of merit for comfort monitoring
* Automatic frost protection

Use of a MQTT broker for reporting and remote control allows the design of this sensor cluster to be controler agnostic. Making it suitabel for use with custom writen controlers, web based controlers and other FOSS controlers ie Node Red. 

The firmware is for use with:-

* Printables, https://github.com/AndyKirby/PrintableParts/tree/master/NoTLamp 
* Electronics, https://github.com/AndyKirby/Electronics/tree/master/NoTLamp 

The design was realised using FOSS throughout. Firmware was writen and debuged via the Arduino IDE with the Arduino ESP8266 extensions 

* Arduino/Genuino IDE, https://www.arduino.cc/en/Main/Software 
* Arduino ESP8266, https://github.com/esp8266/Arduino
* pubsubclient, https://github.com/knolleary/pubsubclient
* Mosquitto, http://mosquitto.org/
* MQTT Spy, http://kamilfb.github.io/mqtt-spy/