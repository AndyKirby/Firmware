/* Master Header file for NoTLamp project */


/* Includes */
#include "secrets.h" // Comment this out if secrets.h is not present
#include <NeoPixelBus.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include "DHT22.h"


/* Defines */
#define DEBUG 1 // comment this out to disable debuging info on uart output

// Wifi Stuff
#define RetryPeriod 5000 // number of scheduler slots to do before retrying a failed wifi connection

// Firmware info
#define VERSION "V0.1"

// Scheduler stuff
#define Interval 10000UL // Number of microseconds that each scheduler slot should take to complete
#define NumSlots 200 // Number of scheduler slots, NumSlots * Interval equals number of seconds for one cycle

// Neopixel stuff
#define colorSaturation 128

// Lamp stuff
#define AlarmPeriod 4

// DHT22 stuff
#define DHTPIN 0          // DHT22 connected to gpio0
#define DHTTYPE DHT22     // Calls are to DHT22 type sensor not DHT11

// Neopixel stuff
#define NumPixels 16 // this assumes min 4 pixels, making it smaller will cause a failure
#define PixelPin 2    // make sure to set this to the correct pin, ignored for Esp8266


/* Type definitions */
// Lamp states
enum LampStates {
  Off,
  SoftOnStart,
  SoftOnChange,
  SoftOnColorStart,
  SoftOnColourChange,
  SoftOffStart,
  SoftOffChange,
  HardOn,
  HardOnColor,
  HardOff,
  AlarmMark,
  AlarmSpace,
  FakeTv,
  On
};


/* Global Constants */
// Wifi and MQTT stuff
#ifndef SECRETS
// put your own specific usernames, passwords and secrets etc here

// Wifi network credentials
const char* wifissid = "MyWifiSSID";   // Wifi noetwork name or ESSID/SSID
const char* wifipwd = "MyWififPassword";   // Wifi password

// MQTT server credentials
const char* nodeprefix = "NoTLamp-";   // Prefix for node name rest is mac address
const char* mqttsrvr = "192.168.1.x";    // Ip address of mqtt broker
const char* mqttuid = "MyMQTTBrokerUser";   // mqtt broker username
const char* mqttpwd = "MyMQTTBrokerPassword"; // mqtt broker password

// MQTT topics for the node
const char* topic_node = "MyNoT/WhereEver/NoTLamp/node";   // topic name for LWT node up/down notifications
const char* topic_temperature = "MyNoT/WhereEver/NoTLamp/sensor/temp";   // topic name for temperature readings
const char* topic_humidity = "MyNoT/WhereEver/NoTLamp/sensor/humid";   // topic name for humidity readings
const char* topic_temperatureindex = "MyNoT/WhereEver/NoTLamp/sensor/tempidx";   // topic name for calculated tmperature index
const char* topic_dewpoint = "MyNoT/WhereEver/NoTLamp/sensor/dewpt";   // topic name for calculated dewpoint
const char* topic_light = "MyNoT/WhereEver/NoTLamp/sensor/light";   // topic name for light level readings
const char* topic_sound = "MyNoT/WhereEver/NoTLamp/sensor/sound"; // topic name for sound level readings
const char* topic_motion = "MyNoT/WhereEver/NoTLamp/sensor/motion"; //topic name for motion detected readings
const char* topic_lamp = "MyNoT/WhereEver/NoTLamp/sensor/lamp"; //topic name for what the lamp is currently doing
const char* topic_lampdo = "MyNoT/WhereEver/NoTLamp/actuator/lamp"; //topic name for making the lamp do things
#endif


/* prototypes */
void DoLamp(void); // Lamp functions state machine
void callback(char*, byte*, unsigned int); // arrived message callback subroutine
void publish_temperature(void); // publish temeperature to MQTT broker
void publish_humidity(void); // publish humidity to MQTT broker


/* Global Variables and objects*/
// Scheduler stuff
unsigned long nextturn; // variable that holds the terminal millis count for a scheduler slot
int slot = 1; // slot counter

// Neopixel stuff
NeoPixelBus<NeoGrbFeature, NeoEsp8266Uart800KbpsMethod> strip(NumPixels, PixelPin);
RgbColor red(colorSaturation, 0, 0);
RgbColor blue(0, 0, colorSaturation);
RgbColor white(colorSaturation);
RgbColor black(0);

//Lamp Stuff
LampStates LampState = Off;
int AlarmTime = AlarmPeriod;
RgbColor NowColour = black;

// Wifi Stuff
bool Timedout = false;
int RetryTime = 0;
WiFiClient wifiClient;

// MQTT Stuff
PubSubClient client(mqttsrvr, 1883, callback, wifiClient);
String NodeName = "";

// DHT22/AM2302 Stuff
DHT22 dht(DHTPIN); // Initialize DHT sensor object
float temperature = 0.0; // the current temperature according to DHT22
float humidity = 0.0; // the current humidity according to DHT22

