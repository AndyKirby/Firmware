/*    
   NoTLamp an instrument cluster and lamp that forms a node of a Network of Things (NoT)
   Note: This code is writen for the esp8266-07 with 512K flash
   Author: Andy Kirby, andy@kirbyand.co.uk, www.kirbyand.co.uk
   License: See license file where this code came from, https://github.com/AndyKirby/Firmware

   Pin alocations:-
   1,Reset   - Reset
   2,ADC     - Diode Multiplexed ADC in
   3,CH_PD   - Pulled High
   4,GPIO16  - Sound Analogue multiplex out
   5,GPIO14  - PIR1 in
   6,GPIO12  - PIR4 in
   7,GPIO13  - PIR5 in
   8,Vcc
   9,Gnd
   10,GPIO15 - LDR/Light Analogue multiplex out (pulls low when not driven)
   11,GPIO2  - NeoPixel Bus Data out
   12,GPIO0  - DHT22 Temp/Humidity in/out (pulls high when not driven)
   13,GPIO5  - PIR3 in
   14,GPIO4  - PIR2 in
   15,RxD    - Serial Monitor and programming
   16,TxD    - Serial Monitor and programming

   Libraries:-
   ESP8266 Wifi
   NeoPixelBus, https://github.com/Makuna/NeoPixelBus
   ** PubSubCLient, https://github.com/knolleary/pubsubclient
   PubSubClient, Imroy, https://github.com/Imroy/pubsubclient
   DHT, Adafruit DHT library for DHT22/AM2302, plundered for routines and then made scheduler friendly
   Dew point algorthms pilfered from PietteTech DHT library, https://github.com/chaeplin/PietteTech_DHT-8266/blob/master/PietteTech_DHT.cpp

   Notes:-
   PIR's are referenced clockwise (to the right) from the first which is the front and opposite the cable entry.
*/

#include "NoTLamp.h"


void setup() {
   int startdelay;
   uint8_t mac[6];

   // Generate client name and LWT messages based on MAC address and last 8 bits of microsecond counter
   WiFi.macAddress(mac);   
   NodeName = nodeprefix + macToStr(mac);

   // Initialise uart
   Serial.begin(115200);

   // show we are alive and give out some useful info
   Serial.printf( "\nNoTLamp Firmware Booted\n" );
   Serial.printf( "Version: %s, %s, %s\n", VERSION, __DATE__, __TIME__ );
   Serial.printf( "Sketch size: %u\n", ESP.getSketchSize() );
   Serial.printf( "Free size: %u\n", ESP.getFreeSketchSpace() );
   Serial.printf( "Heap: %u\n", ESP.getFreeHeap() );
   Serial.printf( "Boot Vers: %u\n", ESP.getBootVersion() );
   Serial.printf( "CPU: %uMHz\n", ESP.getCpuFreqMHz() );
   Serial.printf( "SDK: %s\n", ESP.getSdkVersion() );
   Serial.printf( "Chip ID: %u\n", ESP.getChipId() );
   Serial.printf( "Flash ID: %u\n", ESP.getFlashChipId() );
   Serial.printf( "Flash Size: %u\n", ESP.getFlashChipRealSize() );
   Serial.printf( "Nodename: %s\n", NodeName.c_str() );

   // initialise neopixel strip
   strip.Begin();
   strip.Show();
   Serial.printf("Neopixels: Off\n");

   // initialise the DHT sensor
   dht.begin();

   // connect to the wifi access point
   WifiConnect();

   // connect to the MQTT Broker if the wifi connected ok
   if(WiFi.status() == WL_CONNECTED){
      BrokerConnect();
   }   
   
   // put random delay in here between 0 and 20 seconds to lower probability
   // of all nodes talking at the same time after a power cut
   randomSeed(micros());
   startdelay = (int) random(0,21);
   Serial.printf("Start Delay: %d Seconds\n",startdelay);
   delay(startdelay * 1000);
   
   // Calculate when the first scheduler slot's interval should finish
   Serial.printf("Scheduler Interval: %duS\n", Interval);
   Serial.printf("Scheduler Slots: %d\n", NumSlots);
   Serial.printf("Running Scheduler......\n");
   nextturn = micros() + Interval;
   
   return;
}


void loop() {

   // reset state machine to the begining if it has hit the end
   if(slot > NumSlots) {
     slot = 1;
   }

   // Reconnect to wifi if connection is lost
   if(WiFi.status() != WL_CONNECTED){
      // connect again to wifi can take a little while  
      WifiConnect();
      // reset the loop time as this will have taken more than 1 slot to do
      nextturn = micros() + Interval;  
   }

   // Reconnect to broker if conection is lost but wifi is connected
   if(WiFi.status() == WL_CONNECTED){
      if(!client.connected()) {
         // connect again to broker can take a little while
         BrokerConnect();
         // reset the loop time as this will have taken more than 1 slot to do
         nextturn = micros() + Interval;
      }   
   }
    
   // Scheduler state machine, slots are Interval micro seconds long
   // Any slot needing more than one Interval should recalculate nextturn before carrying out it's tasks
   switch(slot) {
      case(1):
         // start a DHT22 read cycle, wait 20mS before attempting to read
         dht.wakeup();        
         break;
      case(3):
         // read the temperature and humidity data from the DHT sensor, we should do this only once every 2 seconds max
         dht.read();
         break;
      case(4):
         // publish temperature if connected and the DHT read was good
         publish_temperature();
         break;   

      case(10):
         Serial.printf("Tik\n");
         break;
            
      case(24):
         // select the neopixel strip settings in the pixel buffer
         DoLamp();
         break;
      case(25):
         // write the neopixel strip buffer to the strip
         strip.Show();
         break;
        
      case(49):
         // select the neopixel strip settings in the pixel buffer
         DoLamp();
         break;
      case(50):
         // write the strip buffer to the strip
         strip.Show();
         break;

      case(54):
         // publish humidity if connected and the DHT read was good
         publish_humidity();
         break;

      case(74):
         // set the neopixel strip settings in the pixel buffer
         DoLamp();
         break;        
      case(75):
         //write the pixel buffer to the strip
         strip.Show();
         break;

      case(99):
         // set the neopixel strip settings in the pixel buffer
         DoLamp();
         break;  
      case(100):
         //write the pixel buffer to the strip
         strip.Show();
         break;

      case(104):
         // publish temperature index if connected and the DHT read was good
         publish_temperature_index();
         break;    
         
      case(110):
         //useful visual diagnostic
         Serial.printf("Tok\n");
         break;
        
      case(124):
         // set the neopixel strip settings in the pixel buffer
         DoLamp();
         break;  
      case(125):
         //write the pixel buffer to the strip
         strip.Show();
         break;

      case(149):
         // set the neopixel strip settings in the pixel buffer
         DoLamp();
         break;  
      case(150):
         //write the pixel buffer to the strip
         strip.Show();
         break;

      case(154):
         // publish dewpoint if connected and the DHT read was good
         publish_dewpoint();
         break;          

      case(174):
         // set the neopixel strip settings in the pixel buffer
         DoLamp();
         break;  
      case(175):
         //write the pixel buffer to the strip        
         strip.Show();
         break;
         
      case(199):
         // set the neopixel strip settings in the pixel buffer
         DoLamp();
         break;        
      case(200):
         //write the pixel buffer to the strip
         strip.Show();
         break;         
      default:
         // As this is a sparse scheduler we need this for the slots that are not filled
         break;
   }

   if( nextturn - micros() > Interval ) {
      // tell somone that the timing is gettign screwed up
      // idealy this should never happen, coding intervention is required
      // slots needing more than one interval to complete their tasks should extend nextturn
      Serial.printf("Overrun: %d by %d\n",slot, micros() - nextturn);
      nextturn = micros();
   } else {
      while( nextturn - micros() <=  Interval ) {
         // waiting for end of slot
         // Do sleeps/wifi/whatever in here
         yield();
         //check MQTT connection and messages
         MQTTMaintenance();
      }
   }
   
  
   // Calculate end of next cycle time  
   nextturn += Interval;

   //increment slot counter
   slot++;

   return;

}


/* Code for the lamp functions state machine with states/modes as per the enum LampStates
   Allows for a complex set of lamp behaviours and basic animations.
*/
void DoLamp(){
   switch(LampState){
      case(Off):
         // do nothing, the lamp remains off, and we rest here until the state is changed
         // check for movement to see if we should turn it on
         // check if it is dark enough to turn it on
         break;
      case(SoftOnStart):
         strip.ClearTo(black);
         LampState = SoftOnChange;        
         break;
      case(SoftOnChange):
         break;
      case(SoftOnColorStart):
         strip.ClearTo(black);
         LampState = SoftOnColourChange;
         break;
      case(SoftOnColourChange):
         LampState = On;
         break;
      case(SoftOffStart):
         LampState = SoftOffChange;
         break;
      case(SoftOffChange):
         LampState = Off;
         break;
      case(HardOn):
         strip.ClearTo(white);
         LampState = On;
         break;
      case(HardOnColor):
         strip.ClearTo(NowColour);
         LampState = On;
         break;
      case(HardOff):
         strip.ClearTo(black);
         LampState = Off;
         break;
      case(AlarmMark):
         strip.ClearTo(blue);
         AlarmTime--;
         if(AlarmTime<=0) {
            AlarmTime = AlarmPeriod;
            LampState = AlarmSpace;          
         }
         break;
      case(AlarmSpace):
         strip.ClearTo(red);
         AlarmTime--;
         if(AlarmTime<=0) {
            AlarmTime = AlarmPeriod;
            LampState = AlarmMark;
         }              
         break;
      case(FakeTv):
         // implement fake TV algorithm here
         break;
      case(On):
         // do nothing,the lamp remains as on as it is, and we rest here until the state is changed
         // check for lack of occupation to see if we should turn it off
         // check if it is light enough to turn it off  
         break;  
   }
   
   return;
}


// arrived message callback subroutine
void callback(char* topic, byte* payload, unsigned int length) {

  // printout message till we work out what to do with it.
  Serial.printf("Message arrived [%s,%d]",topic,length);
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
  
   return;
} 

// Convert 6 byte mac address to text string subroutine
String macToStr(const uint8_t* mac)
{
   String result;
   
   for (int i = 0; i < 6; ++i) {
      result += String(mac[i], 16);
   }
   
   return result;
}

// Connect to Wifi subroutine
void WifiConnect() {
  int timeout = 30;

  if(!Timedout || (RetryTime == 0)) {
     Serial.printf("Wifi connecting to %s\n",wifissid);
     WiFi.mode(WIFI_STA);
     WiFi.begin(wifissid, wifipwd);
     while ((WiFi.status() != WL_CONNECTED) && (timeout--)) {
        delay(1000);
        Serial.printf(".");
     }
     Serial.printf("\n");
     if( WiFi.status() == WL_CONNECTED ) {
        Serial.print("WiFi connected with IP ");
        Serial.println(WiFi.localIP());
        Timedout = false;
        RetryTime = 0;
     } else {
        Serial.printf("WIFI connect failed !\n");
        Timedout = true;
        RetryTime = RetryPeriod;
     }
  } else {
     RetryTime--;
  }
  
  return;
}

// Connect to MQTT Broker subroutine
void BrokerConnect() {
   String lwtUp;
   String lwtDn;
  
   lwtUp = NodeName + " is alive";
   lwtDn = NodeName + " is dead"; 
  
   Serial.printf("Connecting to broker %s as %s\n", mqttsrvr, mqttuid);

   if (client.connect((char*)NodeName.c_str(), mqttuid, mqttpwd, topic_node, 0, 0,  (char*)lwtDn.c_str())) {
      Serial.printf("Connected to MQTT broker\n");
      Serial.printf("LWT topic is: %s\n", topic_node);
      if (client.publish(topic_node, (char*)lwtUp.c_str())) {
         Serial.printf("Published to lwt ok\n");
      } else {
         Serial.printf("Published to lwt failed\n");
         abort();        
      }
   } else {
      Serial.printf("MQTT connect failed, code %d, reset and try again...\n", client.state());
      abort();
   }

   if(client.subscribe(topic_lampdo)){
      Serial.printf("Subscribed to topic: %s\n",topic_lampdo); 
   } else {
      Serial.printf("Failed to subscribe to topic: %s\n",topic_lampdo);
   }
  
   return; 
} 


// Process incoming messages and maintain server conenction
// Does not block if client is not connected
void MQTTMaintenance() {
   if(client.connected()) {
      client.loop();
   } 

   return;
}


// Publish Humidity to MQTT broker
// Does not block if client is not conencted
void publish_humidity(){
   float humidity;
   String payload;
   int result;

   
   if(client.connected()) {
      humidity = dht.readHumidity();
      if(!isnan(humidity)) {
         payload = humidity;
         result = client.publish(topic_humidity, (char*)payload.c_str());
         if (!result) {
            Serial.printf("Failed to publish humidity!, result %d\n", result);
         }
      } else {
        Serial.printf("Failed to read humidity from DHT sensor!\n");
      }
   }
   
   return;
}


// Publish Temperature to MQTT broker
// Does not block if client is not conencted
void publish_temperature(){
   float temperature;
   String payload;
   int result;

   if(client.connected()) {
      temperature = dht.readTemperature();
      if(!isnan(temperature)) { 
         payload = temperature;
         result = client.publish(topic_temperature, (char*)payload.c_str());
         if (!result) {
            Serial.printf("Failed to publish temperature!, result %d\n", result);
         }
      } else {
         Serial.printf("Failed to read temperature from DHT sensor!\n");
      }
   }

   return;
}

// Publish temperature index to MQTT broker
// Does not block if client is not connected
void publish_temperature_index() {
   float temperature;
   float humidity;
   String payload;
   int result;

   if(client.connected()) {
      temperature = dht.readTemperature();
      humidity = dht.readHumidity();
      if(!isnan(temperature) || !isnan(humidity)) {
         payload = dht.computeHeatIndex(temperature, humidity, false);
         result = client.publish(topic_temperatureindex,  (char*)payload.c_str());
         if (!result) {
            Serial.printf("Failed to publish temperature index!, result %d\n", result);
         }
      }
   }

   return;
}


// Publish dew point to MQTT broker
// Does not block if client is not connected
void publish_dewpoint() {
   float temperature;
   float humidity;
   String payload;
   int result;

   if(client.connected()) {
      temperature = dht.readTemperature();
      humidity = dht.readHumidity();
      if(!isnan(temperature) || !isnan(humidity)) {
         payload = dht.getDewPoint(temperature, humidity);
         result = client.publish(topic_dewpoint,  (char*)payload.c_str());
         if (!result) {
            Serial.printf("Failed to publish dewpoint!, result %d\n", result);
         }
      }
   }
   return;
}

  
// Light read and report subroutine
void do_light(){
   float result;
   String light;
     
   // light as lux
   result = 1.0;
   if(!isnan(result)){
      light = result;
      if(client.publish(topic_light, (char*)light.c_str())) {
         Serial.print("Light: ");
         Serial.print(light);
         Serial.println("Lx");        
      } else {
         Serial.println("Failed to publish light level!");
      }
   } else {
      Serial.println("Failed to read light level from light sensor!");
   }

   return;  
}


// Sound read and report subroutine
void do_sound(){
   float result;
   String sound;
     
   // sound as ?
   result = 1.0;
   if(!isnan(result)){
      sound = result;
      if(client.publish(topic_sound, (char*)sound.c_str())) {
         Serial.print("Sound: ");
         Serial.print(sound);
         Serial.println(" ");        
      } else {
         Serial.println("Failed to publish sound level!");
      }
   } else {
      Serial.println("Failed to read sound level from sound sensor!");
   }

   return;  
}




