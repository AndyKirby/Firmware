/* DHT22 library

MIT license
Taken form original written by Adafruit Industries
*/
#ifndef DHT22_H
#define DHT22_H

#if ARDUINO >= 100
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif

#define IRAM0     __attribute__((section(".iram0.text")))


class DHT22 {
  public:
   DHT22(uint8_t pin);
   void begin(void);
   void wakeup(void);
   bool goodRead(void);
   float readTemperature();
   float convertCtoF(float);
   float convertFtoC(float);
   float computeHeatIndex(float temperature, float percentHumidity, bool isFahrenheit=true);
   float readHumidity();
   boolean read(bool force=false);
   double getDewPoint(float _temp, float _hum);
   double getDewPointSlow(float _temp, float _hum);

 private:
  uint8_t data[5];
  uint8_t _pin;
  uint32_t _lastreadtime, _maxcycles;
  bool _lastresult;

  uint32_t expectPulse(bool level);

};

class InterruptLock {
  public:
   InterruptLock() {
    noInterrupts();
   }
   ~InterruptLock() {
    interrupts();
   }

};

#endif
