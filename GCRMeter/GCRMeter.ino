/*
  Geiger Counter Ratemeter
  
 Author: Andy Kirby andy@kirbyand.co.uk
 Libraries: Arduino, LiquidCrystal,  http://www.arduino.cc/en/Tutorial/LiquidCrystal David A Mellis et al
 
 A capable counter rate meter for use with Geiger Muller tubes etc. May be used with anything else that can be adapted.
 Intended for bench and field use. Features a software configurable tube bias supply and up to 1 Million counts per second range.
 Displays Bias Voltage, CPS, CPM, Trends, and a simple auto scaling scroling graph of the count deltas. Some of the features are specificaly
 created for searching out hotspots in the field under adverse high background conditions.
 
 The Arduino LCD library has a bunch of delays writen into it making LCD updates costly in terms of processor useage.
 Task scheduling has been used to smooth out the per cycle loading. It may be neccesary to do somethign with the LCD library
 If more of the wishlist is to be realised.
 
 Code and idea for T1 timer/counter config and useage was shamelesly borowed from:-
 http://interface.khm.de/index.php/lab/experiments/theremin-as-a-capacitive-sensing-device/
 Thanks for this part then to: Martin Nawrath, nawrath@khm.de
 
 WishList:
   Temperature
   Datalogging to SD card
   Remote serial control
   Battery Indicator and charging
   On Off switch
   Sleep Mode
 
 Pin Allocation:-
 
 168, Arduino,   GCRMeter.
 ---------------------------
 1,   Reset,     Reset.
 2,   RX/D0,     Serial RX.
 3,   TX/D1,     Serial TX.
 4,   D2,        LCD D7
 5,   D3/PWM,    LCD D6
 6,   D4,        LCD D5
 7,   Vcc,       +5V
 8,   Gnd,       Gnd
 9,   Xtal,      Xtal
 10,  Xtal,      Xtal
 11,  D5/PWM,    Pulse Counting Input N.B. CMOS/TTL
 12,  D6/PWM,    LCD D4
 13,  D7,        LCD E
 14,  D8,        LCD RS
 15,  D9/PWM,    LCD Vo/Vlcd Contrast PWM
 16,  D10/PWM,   Tube Bias Drive PWM
 17,  D11/PWM/mosi, 
 18,  D12/miso, 
 19,  D13/sck,
 20,  Vcc,       +5v
 21,  ARef,      ???
 22,  Gnd,       Gnd
 23,  A0/D14,    Debug LED
 24,  A1/D15,    
 25,  A2/D16,    
 26,  A3, 
 27,  A4/SDA,    TWI SDA to RTC and Temp Sensor
 28,  A5/SCL,    TWI SCL to RTC and Temp Sensor
 
 Notes:
 * LCD R/W pin to ground as we never read it
 * 10K Pot ends to +5V and ground, wiper to LCD VO pin to set viewing angle

 */


/* includes */
#include <LiquidCrystal.h>


/* Constants */
#define DebugPin 14    // Digital Pin LED is connected to
#define SpkrPin 9    // Digital Pin Piezo SPeaker is connected to
#define Interval 50000UL // Number of microseconds that each loop should take to complete
#define SecBuf 20 // size of the cps counts ring buffer for a seconds worth of counts
#define MinBuf 60 // size of the cpm counts ring buffer for a minutes worth of counts
#define HourBuf 60 // size of the cph counts rig buffer for an hours worth of counts
#define LCDBufSz 20 // size of the LCD buffer, half of a full line as no element is bigger than this
#define MinTime 60 // number of seconds in a minute
#define CPSRow 0 // lcd row number for cps count
#define CPSCol 4 // lcd col number for cps count
#define CPMRow 1 // lcd row number for cpm count
#define CPMCol 4 // lcd col number for cpm count
#define CPHRow 0 // lcd row number for cph count
#define CPHCol 28 // lcd col number for cph count
#define GraphRow 0 // lcd row number for sub second graph
#define GraphCol 14 // lcd col number for sub second graph
#define BiasRow 1 // lcd row number for bias voltage reading
#define BiasCol 16 // lcd colnumber for bias voltage reading
#define TempRow 1 // lcd row number for temperature reading
#define TempCol 22 // lcd col number for temperature reading
#define BatRow 1 // lcd row number for battery indicator
#define BatCol 37 // lcd col number for battery indicator
#define TimeRow 1 // lcd row number for date indicator
#define TimeCol 28 // lcd col number for date indicator
#define DegSymbol 0xdf // symbol for degrees as in centigrade
#define BatFullSymbol 0xff // symbol for battery full
#define DGraphSize 40 // number of bytes needed to hold the deltas graph ie 7 per displayed char
#define CGROW 7 // number of rows in a custom graphic character
#define CGCOL 5 // number of colums in a custom graphic character
#define CGBitmapSz 8 // number of bytes needed to represent a custom graphic character


// Globals
LiquidCrystal lcd(8, 7, 6, 4, 3, 2); // initialize the LiquidCrystal library with the numbers of the interface pins and declare lcd object
unsigned long nextturn; // variable that holds the terminal millis count for a loop cycle
char lcdbuff[LCDBufSz]; // character buffer for LCD ie one line of display or some custom CG.
char dgraph[DGraphSize]; // deltas graph ring buffer
int dgraphhead = 0; // index to the current head of the deltas bar graph head
int dgraphdelta = 0; // current delta for graphing
char schedule = 0; // scheduler counter
char minute = 0; // seconds counter to determine when a minute has passed for CPH tasks
unsigned cpstallybuf[SecBuf]; // the cps counts ring buffer for exactly a seconds worth of tallys
int cpsbufin = 0; // index to the first item in the cps tally ring buffer
int cpsbufout = 0; // index to the last item in the cps tally ring buffer
int cpsbufcontents = 0; // how many items are currently in the cps tally ring buffer
unsigned long cpmtallybuf[MinBuf]; // the cpm counts ring buffer for exactly a minutes worth of 1 second tallys
int cpmbufin = 0; // index to the first item in the cpm tally ring buffer
int cpmbufout = 0; // index to the last item in the cpm tally ring buffer
int cpmbufcontents = 0; // how many items are currently in the cpm tally ring buffer
unsigned long cphtallybuf[MinBuf]; // the cph counts ring buffer for exactly an hours worth of 1 minute tallys
int cphbufin = 0; // index to the first item in the cph tally ring buffer
int cphbufout = 0; // index to the last item in the cph tally ring buffer
int cphbufcontents = 0; // how many items are currently in the cph tally ring buffer
unsigned long cpscount = 0; // the current calculated number of counts over the last second
unsigned long lastcpscount = 0; // the last displayed cps count to determine trending
unsigned long cpmcount = 0; // the current calculated number of counts over the last minute
unsigned long lastcpmcount = 0; // the last displayed cpm count to determine trending
unsigned long cphcount = 0; // the current calculated number of counts over the last hour
unsigned long lastcphcount = 0; // the last displayed cph count to determine trending
unsigned tallynow = 0; // current T1 value
unsigned tallylast = 0; // last T1 value read, used for subsecond deltas.
int biasvoltage = 0; // Current tube bias voltage measurement
int temperature = 0; // current temperature measurement
int hours = 16; // current time, hours
int minutes = 30; // current time, minutes
int seconds = 12; // current time, seconds
int days = 1; // current date, days
int months = 4; // current date, months
int years = 1984; // current date years
char batlevel = 10; // current battery level 1-10 ie 10% to 100%
uint8_t cgbitmap[CGBitmapSz]; // The bitmap for building custom Characters 


// Main setup routines
void setup() {
   // set up the LCD's number of columns and rows: 
   lcd.begin(40, 2);

   // initialize the LED pin as an output and make sure it is off.
   pinMode(DebugPin, OUTPUT);
   
   // Draw the things on the display that Don't change  
   paintdisplay();
   
   // Initialise the deltas graph with no change
   initdgraph();
  
   // setup the T1 Pin and counter to count external pulses.
   // hardware counter setup ( refer atmega168.pdf chapter 16-bit counter1)
   TCCR1A=0;                   // reset timer/counter1 control register A
   TCCR1B=0;                   // reset timer/counter1 control register B
   // set timer/counter1 hardware as counter , counts events on pin T1 ( arduino pin 5)
   // normal mode, wgm10 .. wgm13 = 0
   TCCR1B = TCCR1B | CS10 | CS11 | CS12; // External clock source on T1 pin. Clock on rising edge.
   TCCR1B = TCCR1B | 7;        // Counter Clock source = pin T1 , start counting now
   TCNT1=0;                    // Reset Counter 1 value to 0
   
   // setup the serial connection
   Serial.begin(57600, SERIAL_8N1);
      
   // Calculate when the first loops interval should finish ie
   // interval microseconds from now
   nextturn = micros() + Interval;
 
}


// Main Loop or body of application
void loop() {
   // Tasks to carry out every cycle assumes a 1 second epoch divided into 20*50mS cycles
   // this allows counting at over 1M counts per second without overflowing the TC1 counter 
   tallynow = TCNT1; // Read the current tally
   TCNT1=0; // Reset Counter 1 value to 0 
   cpsrunningsum(); // recalculate the running cps sum updates cpscount
   updatedgraph(); // compile the new sub second deltas graph adding in the new count
   printdgraph(); // update the sub second deltas graph on the display
   tallylast = tallynow; // save the current tally for working out the next sub second delta 
  
   // Tasks to carry out on the nth cycle. Not every task needs to run every cycle
   // the tasks are best spread out to keep the loop duration evenly short
   switch(schedule++) {
      case 0:
         writecps(); // format and print the CPS total
         minute++; // add a second to the minute timer
         break;
      case 1:
         writecpstrend(); // show the current trend of the cps counts      
         break;
      case 2:
         cpmrunningsum(); // recalculate the running cpm sum updates cpmcount      
         break;    
      case 3:
         if( minute == MinTime ) {
            cphrunningsum(); // recalculate the running cph sum updates cphcount
         }
         break;
      case 4:
         // once per second serial logging CPS count uses lcdbuff for low mem use
         sprintf(lcdbuff,"CPS:%-8lu",cpscount); // Counts Per Second 
         Serial.println(lcdbuff);
         sprintf(lcdbuff,"BIAS:%-4d",biasvoltage); // Bias voltage for tube
         Serial.println(lcdbuff);
         sprintf(lcdbuff,"TEMP:%-3d",temperature); // Temperature
         Serial.println(lcdbuff);
         break;    
      case 5:
         writecps(); // format and print the CPS total
         break;    
      case 6:
         writecpstrend(); // show the current trend of the cps counts    
         break;    
      case 7:
         writecpm(); // format and print the CPM total      
         break;    
      case 8:
         writecpmtrend(); // show the current trend of the cpm counts   
         break;    
      case 9:
         // print up the the current status to the LCD
         writebat();
         writetemp();
         writebias();
         writetime();
         break;    
      case 10:
         writecps(); // format and print the CPS total   
         break;    
      case 11:
         writecpstrend(); // show the current trend of the cps counts
         break;    
      case 12:
         if( minute == MinTime ) {       
            writecph(); //format and print the cph counts
         }  
         break;    
      case 13:
         if( minute == MinTime ) {    
            writecphtrend(); //show the current trend of the cph counts
         }       
         break;    
      case 14:
         if( minute == MinTime ) {  // once per minute serial logging uses lcdbuff for low mem use
            sprintf(lcdbuff,"TIME:%2d:%2d:%2d",hours,minutes,seconds);
            Serial.println(lcdbuff);            
            sprintf(lcdbuff,"DATE:%2d/%2d/%4d",days,months,years);
            Serial.println(lcdbuff);            
            sprintf(lcdbuff,"BAT:%3d",batlevel);            
            Serial.println(lcdbuff);
         }               
         break;
      case 15:
         writecps(); // format and print the CPS total          
         break;
      case 16:
         writecpstrend(); // show the current trend of the cps counts      
         break;
      case 17:
         break;    
      case 18:
         break;       
      case 19:
         if( minute == MinTime ) {
            minute = 0; // reset the minutes counter
         }       
         schedule = 0; // reset the scheduling counter
         break;       
      default: // Should never actualy do this one unless we have screwed up...
         break;
  }

  digitalWrite(DebugPin, LOW); // Turn off debug LED to ignore waiting part of loop  
  // Busy wait for what ever is left of this loop's interval to expire
  while( nextturn - micros() < Interval ) {
  }
  digitalWrite(DebugPin, HIGH); // Turn on debug LED to time active part of loop with a scope
  
  // Calculate end of next cycle time  
  nextturn += Interval;
}


// Draw all the things that don't change
void paintdisplay() {
   unsigned char cgchars;
    
   // Counts per second display  
   lcd.setCursor(CPSCol - 4, CPSRow);
   lcd.print(F("CPS:"));
   // Counts per minute display
   lcd.setCursor(CPMCol - 4, CPMRow);
   lcd.print(F("CPM:"));
   // Counts per hour display
   lcd.setCursor(CPHCol - 4, CPHRow);
   lcd.print(F("CPH:"));
   // Sub-second graph
   lcd.setCursor(GraphCol - 1, GraphRow);   
   lcd.print(F(">........<"));
   lcd.setCursor(GraphCol, GraphRow);
   for( cgchars = 0; cgchars < 8; cgchars++) lcd.write(byte(cgchars));
   // Status Line
   // Tube Bias Voltage 
   lcd.setCursor(BiasCol - 1, BiasRow);
   lcd.print(F("|....V|"));
   // Temperature Sensor
   lcd.setCursor(TempCol - 1, TempRow);
   lcd.print(F("|....C|"));
   // Battery indicator
   lcd.setCursor(BatCol - 1, BatRow);   
   lcd.print(F("|.|"));
   // Date
   lcd.setCursor(TimeCol - 1, TimeRow);
   lcd.print(F("|hh:mm:ss|"));
  
}


// Write the current CPS total to the LCD
void writecps() {
  lcd.setCursor(CPSCol, CPSRow);
  sprintf(lcdbuff,"%8lu",cpscount);
  lcd.print(lcdbuff);   
}


// Write the current CPM total to the LCD
void writecpm() {
  lcd.setCursor(CPMCol, CPMRow);
  sprintf(lcdbuff,"%10lu",cpmcount);
  lcd.print(lcdbuff);   
}


//Write the current CPH total to the LCD
void writecph(){
  lcd.setCursor(CPHCol, CPHRow);
  sprintf(lcdbuff,"%12lu",cphcount);
  lcd.print(lcdbuff);
}


// write the current bias voltage measurement to the lcd
void writebias(){
  lcd.setCursor(BiasCol, BiasRow);
  sprintf(lcdbuff,"%4dV",biasvoltage);
  lcd.print(lcdbuff);
}


// write the current temperature measurement to the lcd
void writetemp(){
  lcd.setCursor(TempCol, TempRow);
  sprintf(lcdbuff,"%3d%cC",temperature, DegSymbol);
  lcd.print(lcdbuff);
}  

  
// write the current battery level measurement to the lcd
void writebat(){
  lcd.setCursor(BatCol, BatRow);
  sprintf(lcdbuff,"%c",BatFullSymbol);
  lcd.print(lcdbuff);
}


// write the current time to the LCD
void writetime(){
   lcd.setCursor(TimeCol, TimeRow);  
   sprintf(lcdbuff,"%2d:%2d:%2d",hours, minutes, seconds);
   lcd.print(lcdbuff);            
}
  

// write the arrows showing the current cps trend
// Up, Down or Same
void writecpstrend() {
   lcd.setCursor(CPSCol - 1, CPSRow);
   if( lastcpscount > cpscount ) {
      lcd.print(F("<"));    
   }
   else if ( lastcpscount < cpscount ) {
      lcd.print(F(">"));      
   }
   else {
      lcd.print(F(":"));       
   }
   lastcpscount = cpscount;   
}


// write the arrows showing the current cpm trend
// Up, Down or Same
void writecpmtrend() {
   lcd.setCursor(CPMCol - 1, CPMRow);
   if( lastcpmcount > cpmcount ) {
      lcd.print(F("<"));    
   }
   else if ( lastcpmcount < cpmcount ) {
      lcd.print(F(">"));      
   }
   else {
      lcd.print(F(":"));       
   }
   lastcpmcount = cpmcount;
}


// write the arrows showing the current cph trend
// Up, Down or Same
void writecphtrend() {
   lcd.setCursor(CPHCol - 1, CPHRow);
   if( lastcphcount > cphcount ) {
      lcd.print(F("<"));    
   }
   else if ( lastcphcount < cphcount ) {
      lcd.print(F(">"));      
   }
   else {
      lcd.print(F(":"));       
   }
   lastcphcount = cphcount;
}


// initialise the dbgraph to the midpoint.
void initdgraph() {
  int graph;
  for(graph = 0; graph < DGraphSize; graph++) { // for every element of the array
     dgraph[graph] = 3; // initialise to median or no change value
  }     
}


// calculate and store the current delta in the dgraph ring buffer
// valid values are 0 thru 7 with 3 being the no change or median point.
// no change readings move the delta back towards the 3 or median point
// it is effectively a damped derivative or difference indicator
void updatedgraph() {  
  if(tallynow == tallylast) {
     if( dgraphdelta > 3 ) dgraphdelta--;
     if( dgraphdelta < 3 ) dgraphdelta++;
  } else if( tallynow > tallylast ) {
     if( dgraphdelta > 0 ) dgraphdelta--;
  } else {
    if( dgraphdelta < 7 ) dgraphdelta++;
  }
  dgraph[dgraphhead] = dgraphdelta; // put new delta into ring buffer
  dgraphhead == (DGraphSize -1) ? dgraphhead = 0 : dgraphhead++;  // increment head of ring buffer mod DGraphSize
}


// print the graph on the LCD converting the deltas in the ring buffer
void printdgraph() {
   int cgchar;
   char column;
   int clean;
   int offset;
   int row;
  
   for(cgchar = 0; cgchar < 8; cgchar++) { // for every custom character of the 8
     // clear the cgbitmap
     for(clean = 0; clean < CGBitmapSz; clean++ ) {
        cgbitmap[clean] = 0x00;
     } 
     // build the new bitmap for this cg  and reload it
     for( column = 0; column < 5; column++) { //iterate over 5 values from the deltas array one per bitmap column
        offset = dgraphhead - (cgchar * 5 + column + 1); // calculate the offset relative to dgraphead
        if( offset < 0 ) offset += DGraphSize; // allow for wrap around
        row =  dgraph[offset]; //pull the indicated delta from the deltas ring buffer
        switch( column ) {
           case 0: // set bit 5 of row given by deltas value
              cgbitmap[row] |= 0x10;
              break;              
           case 1: // set bit 4 of row given by deltas value
              cgbitmap[row] |= 0x08;
              break;              
           case 2: // set bit 3 of row given by deltas value
              cgbitmap[row] |= 0x04;
              break;              
           case 3: // set bit 2 of row given by deltas value
              cgbitmap[row] |= 0x02;
              break;              
           case 4: // set bit 1 of row given by deltas value
              cgbitmap[row] |= 0x01;
              break;              
           default: // should never do this unless we have screwed up
              break;
        }
     }
     lcd.createChar(cgchar, cgbitmap); // reload the new custom character bitmap into the LCD
  }
}


// put tallynow into the cps ring buffer and update the running cps sum
// ring buffer is exactly 1 seconds worth of tallys long.
void cpsrunningsum() {
   cpscount += tallynow; // Add the new value to the CPS total  
   if( cpsbufcontents == SecBuf ) { // check that buffer has a seconds worth of contents
      cpscount -= cpstallybuf[cpsbufout]; // read item indexed by cpsbufout from ring buffer subtract from cps total
      cpsbufout == (SecBuf -1) ? cpsbufout = 0 : cpsbufout++;  // increment cpsbufout index mod SecBuf
   } else { // if not we are filling the buffer
      cpsbufcontents++;
   }
   cpstallybuf[cpsbufin] = tallynow; // overwrite value pointed to by cbufin with tallynow.      
   cpsbufin == (SecBuf -1) ? cpsbufin = 0 : cpsbufin++;// increment cpsbufin mod SecBuf
}


// put curent cps into the cpm ring buffer and update the runnin cpm sum
// ring buffer is exactly 1 minutes worth of cps values long.
void cpmrunningsum() {
   cpmcount += cpscount; // Add the new CPS value to the CPM total
   if( cpmbufcontents == MinBuf ) { // check that buffer has a minutes worth of contents
      cpmcount -= cpmtallybuf[cpmbufout]; // read item indexed by cpmbufout from ring buffer subtract from cpm total
      cpmbufout == (MinBuf -1) ? cpmbufout = 0 : cpmbufout++;  // increment cpmbufout index mod SecBuf
   } else { // if not we are filling the buffer
      cpmbufcontents++;
   }
   cpmtallybuf[cpmbufin] = cpscount; // overwrite value pointed to by cpmbufin with cpscount.      
   cpmbufin == (MinBuf -1) ? cpmbufin = 0 : cpmbufin++;// increment cpmbufin mod MinBuf  
}


// put curent cpm into the cph ring buffer and update the runnin cph sum
// ring buffer is exactly 1 hours worth of cpm values long.
void cphrunningsum() {
   cphcount += cpmcount; // Add the new CPM value to the CPH total
   if( cphbufcontents == HourBuf ) { // check that buffer has an hours worth of contents
      cphcount -= cphtallybuf[cphbufout]; // read item indexed by cphbufout from ring buffer subtract from cph total
      cphbufout == (HourBuf -1) ? cphbufout = 0 : cphbufout++;  // increment cphbufout index mod HourBuf
   } else { // if not we are filling the buffer
      cphbufcontents++;
   }
   cphtallybuf[cphbufin] = cpmcount; // overwrite value pointed to by cpmbufin with cpscount.      
   cphbufin == (HourBuf -1) ? cphbufin = 0 : cphbufin++;// increment cpmbufin mod MinBuf    
}

