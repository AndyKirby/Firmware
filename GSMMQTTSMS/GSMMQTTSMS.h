/*
 *  A MQTT to SMS gateway using the SIM800L GSM modem and an ESP32
 *  Main UART used for serial debug link and second serial link to modem
 *  Debug output serial stream on Serial
 *  SIM800L GSM Modem on Serial1
 */
 
/* Includes */
// either comment this out and use the __SECRETS_H__ block below, or make a secrets.h using the block below as a template
#include "secrets.h" 
#include <Arduino.h>
#include <Wire.h>
#include <WiFi.h>
#include <HardwareSerial.h>
#include <PubSubClient.h>


/* Defines */
#ifndef __SECRETS_H__
   #define MYSSID "MyWifiSSID"
   #define MYWIFIPASSWORD "MyWifiKey"
   #define MYMQTTUSR "MyMQTTUser"
   #define MYMQTTPWD "MyMWQTTPassword"
   #define MQTTSRVR "MyMQTTSrvrIP"
   #define MQTTPRT 1883
   #define OUTTOPIC "TopicTreeTop/mqtt2sms/out"
   #define INTOPIC "TopicTreeTop/mqtt2sms/in"
   #define LWTTOPIC "TopicTreeTop/mqtt2sms/status"
#endif /* __SECRETS_H__ */
#if CONFIG_FREERTOS_UNICORE
   #define ARDUINO_RUNNING_CORE 0
#else
   #define ARDUINO_RUNNING_CORE 1
#endif
//#define MODEMDIAG // comment out this conditional to supress modem diagnostics
#define BUILTIN_LED 13
//#define SerialGSM Serial
#define SerialDBG Serial
#define LWTQOS 0
#define LWTRTN 0
#define LWTMSG "MQTT2SMS Gateway timed out presumed dead"
#define HOSTNAME "mqtt2sms"
#define WIFIRETRIES 10
#define GSMMAX 260 //max line length is 256 including <cr><lf> termination

#define SIM800L_TX                  27
#define SIM800L_RX                  26
#define SIM800L_RST                 5
#define SIM800L_PWKEY               4
#define SIM800L_POWER_ON            23
#define SIM800L_BAUD_RATE           115200

#define HOST_BAUD_RATE              115200

#define IP5306_SDA                  21
#define IP5306_SCL                  22
#define IP5306_ADDR                 0X75
#define IP5306_REG_SYS_CTL0         0x00

// numeric result codes
#define RESULT_OK                   0
#define RESULT_CONNECT              1
#define RESULT_RING                 2
#define RESULT_NOCARRIER            3
#define RESULT_ERROR                4
#define RESULT_NODIALTONE           6
#define RESULT_BUSY                 7
#define RESULT_NOANSWER             8
#define RESULT_PROCEEDING           9


/* function prototypes */
// utility
extern void SetupWifi();
extern unsigned short hash(char const *);
extern void strim(char *);

// MQTT
extern void MQTTsetup();
extern void MQTTreconnect();
extern void MQTTrxmsg(char*, byte*, unsigned int);
extern void MQTTtxmsq(char*);
extern void MQTTstatmsq(char*);

//GSM
extern void SetupSIM800L();
extern bool setPowerBoostKeepOn(int);
extern void ToSIM800L(const char *);
extern int FrmSIM800L(char *);
extern void FlushSIM800L();
extern void FlushTillSIM800L(char);

extern void SndSMS(char *, char *);
extern int OKRespSIM800L();
extern int OKInfoSIM800L();
extern int RespInfoSIM800L();
extern void HandleSIM800L();
extern int UnsolSIM800L(char *);
extern void CCLKusSIM800L(char *);
extern void CREGusSIM800L(char *);
extern void CSQusSIM800L(char *);
extern void CMTIusSIM800L(char *);
extern void CMTusSIM800L(char *);
extern void  PSUTZusSIM800L(char *);





 
