/*
 *  A MQTT to SMS gateway using the SIM800L GSM modem and an ESP32
 *  Main UART used for serial debug link and second serial link to modem
 *  Debug output serial stream on Serial
 *  SIM800L GSM Modem on Serial1
 */

/********************
 * utility routines *
 ********************/

// setup the wifi conenctivity to the prefered NoT
void SetupWifi() {
   int wifiretry = 0;
  
   // set station mode
   WiFi.mode(WIFI_STA);
   // enable automatic reconenction
   WiFi.setAutoReconnect(true);
   // initiate connection attempt  
   SerialDBG.print("Wifi connecting to ");
   SerialDBG.println(MYSSID); 
   WiFi.begin(MYSSID, MYWIFIPASSWORD);
   SerialDBG.print("Waiting for connection");

   // wait for a succesful conenction witha  timeout
   // if we timeout reset the ESP and start again
   while (WiFi.status() != WL_CONNECTED) {
      if(wifiretry > WIFIRETRIES) {
         SerialDBG.println(" ");        
         SerialDBG.println("Connection Failed! Rebooting...");
         delay(1000);
         ESP.restart();
      } else {
        SerialDBG.print(".");
        delay(1000);  
      }
      wifiretry++;
   }
   // annpounce we are connected and give our IP address
   SerialDBG.println(" ");
   SerialDBG.print("IP address: ");
   SerialDBG.println(WiFi.localIP());
   SerialDBG.println("Wifi connected");

   return;
}


// pinched this idea from https://stackoverflow.com/questions/7666509/hash-function-for-string
unsigned short hash(char const *input) { 
    unsigned short result = 0x5555;

    while (*input) { 
        result ^= *input;
        input++;
        result = result << 5;
    }

    return(result);
}


// process the indicated csv string removing unwanted characters
// add a comma delimeter to end, rewriten string will be shorter 
void strim(char *hdr) {
   char *newstr;
   char *crsr;


   // replace unwanted chars with white space
   crsr = hdr;
   while( *crsr != 0x00 ){
      *crsr = (*crsr == '"') ? ' ' : *crsr;
      crsr++;
   }

   // rewrite the string without white space
   crsr = hdr;
   newstr = hdr;
   while( *crsr != 0x00 ){
      if(*crsr != ' ') {
        *newstr = *crsr;
        newstr++;
      }
      crsr++;
   }
   
   // add a comma delimeter to the end
   *newstr = ',';
   newstr++;
   
   // null terminate the rewriten string
   *newstr = 0x00;

   return;
}
