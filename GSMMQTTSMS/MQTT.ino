/*
 *  A MQTT to SMS gateway using the SIM800L GSM modem and an ESP32
 *  Main UART used for serial debug link and second serial link to modem
 *  Debug output serial stream on Serial
 *  SIM800L GSM Modem on Serial1
 */

/**************************
 * MQTT specific routines *
 **************************/

// MQTT setup routine
void MQTTsetup(){
   // set server IP and port
   MQTTclient.setServer(MQTTSRVR, MQTTPRT);
   // attach the rx callback to the client
   MQTTclient.setCallback(MQTTrxmsg);
   // if not already connected connect
   if (!MQTTclient.connected()) {
      MQTTreconnect();
   }

   return;
}

// mqtt reconnect routine to initialy connect thence to restore lost connections
void MQTTreconnect() {
  // Loop until we're reconnected
   while (!MQTTclient.connected()) {
      SerialDBG.printf("Attempting MQTT connection to %s on port %d\n",MQTTSRVR, MQTTPRT);
      MQTTclient.connect(HOSTNAME,MYMQTTUSR,MYMQTTPWD,LWTTOPIC,LWTQOS,LWTRTN,LWTMSG);
      if (MQTTclient.connect(HOSTNAME)) {       
         SerialDBG.println("Connected to MQTT broker");
         // Once connected, publish an announcement...
         MQTTclient.publish(LWTTOPIC, "MQTT2SMS Gateway Online");
         // subscribe to the topic we want to monitor for messages to send
         MQTTclient.subscribe(INTOPIC);
      } else {
         SerialDBG.print("Failed, rc=");
         SerialDBG.print(MQTTclient.state());
         SerialDBG.println(" trying again in 5 seconds"); 
         delay(5000); // Wait 5 seconds before retrying
      }
   }

   return;
}


//MQTT Callback to process received messages
void MQTTrxmsg(char* topic, byte* payload, unsigned int length) {
   unsigned int i;
   char buf[MQTT_MAX_PACKET_SIZE + 1];
   char *message = NULL;
   int msg_length;
   int num_length;

   //ensure there is a long enough message to be viable
   if(length < 10) {
      SerialDBG.println("MQTT message too short to be sendable");
      return;
   }

   // ensure message length is not more than we can cope with
   if(length >= MQTT_MAX_PACKET_SIZE) {
      msg_length =  MQTT_MAX_PACKET_SIZE;
   } else {
      msg_length = length;
   }
  
   // copy the message to a buffer for processing and sending
   strncpy(buf, (const char *) payload, msg_length);
   // make sure the last string char is null
   buf[msg_length] = 0x00;

   SerialDBG.print("SMS to Send: ");
   SerialDBG.println(buf);

   // replace the comma with a null character to delimit the number and message
   // set the message pointer to the start of the message section of buf
   // note this is the first comma so the actual message section can contain commas
   for(i=0;i < msg_length;i++){
      if(buf[i] == ',') {
        buf[i] = 0x00;
        message = &buf[i+1];
        break;
      }
   }

   // sanity check before we try sending anything
   // total thing is in buff as two strings buf points to the number section
   // message points to the message section
   // we can not test for a null message easily if the message was malformed
   // but actual message section length = total message length - number section length
   num_length = strlen(buf);
   if( (msg_length - num_length) < 2 ) {
      SerialDBG.println("SMS message too short to be viable");
      return;     
   }

   // number section must be prefixed with a +
   if(buf[0] != '+') {
      SerialDBG.println("No + prefix on number string");
      return;           
   }

   // actual number must be all numbers and no other characters
   for(i=1;i < num_length;i++) {
      if(!isdigit(buf[i])) {
         SerialDBG.println("Number string contains non digit");
         return;  
      }
   }
   
   // flash the LED as we send the SMS
   digitalWrite(BUILTIN_LED, HIGH);
   SndSMS(buf, message);
   digitalWrite(BUILTIN_LED, LOW);

   return;
}


//MQTT send message in msg to OUTTOPIC topic, copy it to the debug output
void MQTTtxmsq(char* mymsg){
   SerialDBG.print("Publish message: ");
   SerialDBG.println(mymsg);
   MQTTclient.publish(OUTTOPIC, mymsg); 
}


//MQTT send message in msg to LWTTOPIC topic, copy it to the debug output
void MQTTstatmsq(char* mymsg){
   SerialDBG.print("Status message: ");
   SerialDBG.println(mymsg);
   MQTTclient.publish(LWTTOPIC, mymsg); 
}
