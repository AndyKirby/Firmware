/*
 *  A MQTT to SMS gateway using the SIM800L GSM modem and an ESP32
 *  Main UART used for serial debug link and second serial link to modem
 *  Debug output serial stream on Serial
 *  SIM800L GSM Modem on Serial1
 */


/***************************************
 * SIM800L GSM Modem specific routines *
 ***************************************/

// setup SIM800L modem
void SetupSIM800L(){
   bool isOk;
   String info;
 
   // enable the battery controler ic to provide power boost
   Wire.begin(IP5306_SDA, IP5306_SCL);
   isOk = setPowerBoostKeepOn(1);
   info = "IP5306 KeepOn " + String((isOk ? "PASS" : "FAIL"));
   Serial.println(info);
  
   // power cycle the modem
   SerialDBG.println("Turning modem on, please wait");
   // Set-up modem reset, enable, power pins
   pinMode(SIM800L_PWKEY, OUTPUT);
   pinMode(SIM800L_RST, OUTPUT);
   pinMode(SIM800L_POWER_ON, OUTPUT);
   digitalWrite(SIM800L_PWKEY, LOW);
   digitalWrite(SIM800L_RST, HIGH);
   digitalWrite(SIM800L_POWER_ON, HIGH);

   // give it a moment to wakeup
   delay(5000);

   // reset to factory defaults
   SerialDBG.println("Set factory defaults");
   ToSIM800L("AT&F0");
   FlushSIM800L();

   // give it a moment to load the defaults
   delay(5000);

   // set echo off, short numeric result codes
   SerialDBG.println("Set echo off, numeric results");
   ToSIM800L("ATE0V0");
   FlushSIM800L();

   // flush the buffer of intial boot up junk and check modem is responsive
   SerialDBG.println("Attempting setup");
   delay(1000);   
   FlushSIM800L();
   ToSIM800L("AT");
   OKRespSIM800L();
   SerialDBG.println("Modem responsive");

   // enable sync modem time to network time
   SerialDBG.println("Enable syncing modem to network time");   
   ToSIM800L("AT+CLTS=1");
   OKRespSIM800L();

   // enable extended network registration info
   SerialDBG.println("Enable network registration extended info");
   ToSIM800L("AT+CREG=2");
   OKRespSIM800L();

   // Set caller ID on
   SerialDBG.println("Set Caller ID on");
   ToSIM800L("AT+CLIP=1");
   OKRespSIM800L();    

   // Set sms to text mode
   SerialDBG.println("Set SMS to text mode");
   ToSIM800L("AT+CMGF=1");
   OKRespSIM800L();

   // make sure sms character set is GSM
   SerialDBG.println("Set SMS character set to GSM");
   ToSIM800L("AT+CSCS=\"GSM\"");
   OKRespSIM800L();

   // receive live SMS messsages direct from modem
   SerialDBG.println("Set SMS rx direct to ESP32");
   ToSIM800L("AT+CNMI=2,2,0,0,0");
   OKRespSIM800L();  
   
   // read the modems manufacturer info
   ToSIM800L("ATI");
   OKInfoSIM800L();

   // read the modems SIM ID
   SerialDBG.print("IMSI: ");   
   ToSIM800L("AT+CIMI");
   OKInfoSIM800L();

   // read the modems IMEI
   SerialDBG.print("IMEI: ");
   ToSIM800L("AT+CGSN");
   OKInfoSIM800L();   

   // read the local GSM signal strength 
   ToSIM800L("AT+CSQ");
   OKRespSIM800L();

   // check GSM status and connect to network.
   ToSIM800L("AT+CREG?");  
   OKRespSIM800L();

   SerialDBG.println("GSM Modem Setup complete");
 
   return;
}


// turn on the battery to keep GSM power spikes controled 
bool setPowerBoostKeepOn(int en) {
  
    Wire.beginTransmission(IP5306_ADDR);
    Wire.write(IP5306_REG_SYS_CTL0);
    if(en){
       // Set bit1: 1 enable 0 disable boost keep on
       Wire.write(0x37); 
    }else{
       // 0x37 is default reg value
       Wire.write(0x35); 
    }
    
    return Wire.endTransmission() == 0;
}


// Primitive write commands etc to SIM800L modem
void ToSIM800L(const char * To){
  
#ifdef MODEMDIAG  
   SerialDBG.print("SIM800L<");
   SerialDBG.println(To);
#endif
   SerialGSM.println(To);
   
   return;
}


// Primitive read crlf terminated responses from SIM800L modem, returns status of the read
// Note expects short form result codes ATV0 <text><cr><lf> or <numeric code><CR>
int FrmSIM800L(char * Frm){
   int status = 0; //0 for timeout, otherwise number of bytes read in current line

   // read a line from the modem terminated by cr
   status = SerialGSM.readBytesUntil(0x0d, Frm, GSMMAX);
   // ensure there is a null terminating the string, overwrite the cr
   Frm[status] = 0x00;
   // remove any trailing <lf> for this response, from the buffer
   if(SerialGSM.peek()==0x0a){
      SerialGSM.read();
   }
   
#ifdef MODEMDIAG
   if(status != 0){  
      SerialDBG.print("SIM800L>");
      SerialDBG.print(Frm);
      SerialDBG.print("<");
      SerialDBG.println(status);
   } else {
      SerialDBG.println("SIM800L Status> *Timed out*");      
   }
#endif  

   return(status);
}


// flush all junk characters from the rx buffer
void FlushSIM800L(){
  
  while(SerialGSM.available()){
    SerialGSM.read();
  }

  return;
}


// flush all characters from rx buffer until the indicated character
void FlushTillSIM800L(char target){
    int done=0;
    char next;

    while(!done){
       next = SerialGSM.read();
       if(next == target){
          done = 1;
       }
#ifdef MODEMDIAG
          SerialDBG.print("flushSIM800L>");
          SerialDBG.println(next, HEX);
#endif         
    }

    return;
}


// send sms msg string to num string
// https://www.developershome.com/sms/cmgsCommand3.asp
void SndSMS(char *num, char *mymsg) {
  char buf[GSMMAX];

   // set the messagee's phone number
   sprintf(buf, "AT+CMGS=\"%s\"", num);
   ToSIM800L(buf);
   //flush characters from modem until the proceed prompt is encountered
   FlushTillSIM800L(' ');
   // write message to modem
   ToSIM800L(mymsg);
   //flush characters from modem until the proceed prompt is encountered
   FlushTillSIM800L(' ');
   // signal end of message
   ToSIM800L("\x1A");
   //flush characters from modem until the proceed prompt is encountered
   FlushTillSIM800L(' ');
   // flush remainder line by line till an ok or error is received
   OKInfoSIM800L();

   return;
}


// flush the rx buffer line by line till receipt of OK, 0, error or a valid response is recognized
// attempt to decode the response by parsing for known unsolicited codes or errors
int OKRespSIM800L(){
   char reply[GSMMAX];
   int done = 0;

   while(done == 0){
      FrmSIM800L(reply);
      if(strcmp(reply,"0") == 0) {
        done = 1;
      } else if(strcmp(reply,"OK") == 0) {
        done = 1;
      } else {
         done = UnsolSIM800L(reply);
         if(done == -1) {
            SerialDBG.println(reply);
         }
      }
      yield(); 
   }
     
   return(done);
}


// flush the rx buffer line by line to the diag port till receipt of OK, 0 or an error is recognized
int OKInfoSIM800L(){
   char reply[GSMMAX];
   int done = 0;
   
   while(done == 0){
      FrmSIM800L(reply);
      if(strcmp(reply,"0") == 0) {
        done = 1;
      } else if(strcmp(reply,"OK") == 0) {
        done = 1;
      } else if(strstr(reply," ERROR:")) {
        SerialDBG.println(reply);
        done = -1;
      } else {
        SerialDBG.println(reply);
      }
      yield();
   }   
     
   return(done);
}


// read a numeric response code and give info on it, return the numeric value
int RespInfoSIM800L(){
   char reply[GSMMAX];
   int response = 0;
   int numchar = 0;

   numchar = FrmSIM800L(reply);   
   response = atoi(reply);

   switch(response) {
    case RESULT_OK:
       SerialDBG.println("0: Ok");
       break;
    case RESULT_CONNECT:
       SerialDBG.println("1: Connect");
       break;
    case RESULT_RING:
       SerialDBG.println("2: Ring");
       break;
    case RESULT_NOCARRIER:
       SerialDBG.println("3: No carrier");
       break;
    case RESULT_ERROR:
       SerialDBG.println("4: Error");
       break;
    case RESULT_NODIALTONE:
       SerialDBG.println("6: No dialtone");
       break;
    case RESULT_BUSY:
       SerialDBG.println("7: Busy");
       break;
    case RESULT_NOANSWER:
       SerialDBG.println("8: No answer");
       break;
    case RESULT_PROCEEDING:
       SerialDBG.println("9: Proceeding");
       break;
    default:
       SerialDBG.print(numchar);
       SerialDBG.print(": ");
       SerialDBG.print(reply);
       SerialDBG.print(": ");
       SerialDBG.print(response);
       SerialDBG.println(": Unrecognized");
       return(4);    
       break;
   }
   
   
   return(response);
}


// handle SIM800L events
void HandleSIM800L(){
   char lookahead;
   char reply[GSMMAX];
   
   // if there is a response or event to process
   if (SerialGSM.available()) {
      // sneak a peak at the first character  
      lookahead = SerialGSM.peek();
      // if a number it is a numeric result code, otherwise a text result code
      if(isdigit(lookahead)){
         // numeric result code
         RespInfoSIM800L();
      } else {
        // unexpected text result (should be numeric) or an unsolicited event
        FrmSIM800L(reply);
        UnsolSIM800L(reply);
      }
   }
   
   return;
}


// unsolicited GSM event handler/decoder
// Add additional codes in here as needed
// return 0 for unrecognised, +n for recognised event/record, -n for recognised error
int UnsolSIM800L(char * Event){
   char unsolID[20] = "Unrecognized: ";
   char *payload;
   int result = 0;
   unsigned short hashval;

   // if the unsolicited id delimiter is in the event string
   if(strstr(Event, ": ") || strstr(Event,"ERROR:")) {
      // pull out the ID code prefix
      sscanf(Event, "%18s:",unsolID);
      // set payload pointer to just past the ID code
      payload = Event + strlen(unsolID);
      // advance payload pointer past any leading spaces
      while(*payload == ' ') {
        payload++;
      }
      // calculate a simple sparse hash for the unsolicted event code
      hashval = hash(unsolID);

      // lookup event code actions for the matching hash
      switch(hashval) {
         case(0x2340): //+CMTI: received sms storage location for retrieval
            CMTIusSIM800L(payload);
            result = 1;
            break;
         case(0x2B40): //+CCLK: Realtime Clock time and date, reformat/decode Event
            CCLKusSIM800L(payload);
            if(valid_tstamp) {
               MQTTstatmsq(Event);
            }
            result = 1; 
            break;
         case(0x6F40): //*PSUTTZ: network time sysnchronization at registration Event
            PSUTZusSIM800L(payload);
            result = 1; 
            break;
         case(0x9B40): //+CREG: Network Registration, reformat/decode Event
            CREGusSIM800L(payload);
            MQTTstatmsq(Event);
            ToSIM800L("AT+CSQ"); //triger signal strength status
            result = 1; 
            break;               
         case(0xC340): //+CSQ: Signal Strength/Quality, reformat/decode Event 
            CSQusSIM800L(payload);
            MQTTstatmsq(Event);
            ToSIM800L("AT+CCLK?"); //triger time and date status
            result = 1;      
            break;
         case(0xCB40): //+CPMS: SMS storage locations
            result = 1; 
            break;            
         case(0xD740): //+CMT: an actual received sms forwarded straight to the ESP32 from the GSM modem
            CMTusSIM800L(payload);
            result = 1;
            break;
         case(0xBE60): //+CMS CMS Errors, return as a recognized error
            result = -1; 
            break;
         default:      //Unrecognized copy it and the hash to the debug port for refference
            SerialDBG.print("Unrcognized unsol: ");
            SerialDBG.print(unsolID);
            SerialDBG.print(",");
            SerialDBG.print(hashval, HEX);
            SerialDBG.print(",");
            SerialDBG.println(Event);
            result = 0; 
            break;
      }
   }

   return(result);
}


// takes the CCLK payload and prints out a sensible time date format
void CCLKusSIM800L(char *payload){
   int Yr;
   int Mn;
   int Dy;
   int Hr;
   int Mi;

   //"18/02/25,23:00:23+0"
   sscanf(payload,"\"%d/%d/%d,%d:%d",&Yr,&Mn,&Dy,&Hr,&Mi);
   SerialDBG.print("Date: ");
   SerialDBG.print(Dy);
   SerialDBG.print("/");
   SerialDBG.print(Mn);
   SerialDBG.print("/");
   SerialDBG.println(Yr);
   SerialDBG.print("Time: ");
   SerialDBG.print(Hr);
   SerialDBG.print(":");
   SerialDBG.println(Mi);
   
   return;
}


// takes the CREG payload and prints out a sensible interpretation
void CREGusSIM800L(char *payload){
   unsigned int reg;
   unsigned int stat;
   unsigned int lac;
   unsigned int cellID;
   int oddnum;
   int items;

   // parse the result code from a +CREG solicited query
   items = sscanf(payload,"%u,%u,\"%x\",\"%x\",%d", &reg, &stat, &lac, &cellID, &oddnum);

   if(items != 1){
      SerialDBG.print("Network Registration reporting: ");
      switch(reg){
         case(0):
            SerialDBG.println("Unsolicited disabled");
            break;   
         case(1):
            SerialDBG.println("Unsolicited enabled");
            break;
         case(2):
            SerialDBG.println("Extended unsoliticed enabled");
            break;
         default:
            SerialDBG.println("Unknown");
            break;
      }
   } else {
      // reparse the result code from a +CREG unsolicited update
      items = sscanf(payload,"%u,\"%x\",\"%x\",%d", &stat, &lac, &cellID, &oddnum);
   }
   
   SerialDBG.print("Network Registration Status: ");
   switch(stat){
      case(0):
         SerialDBG.println("Not registered, not currently searching for a new operator to register to");
         break;   
      case(1):
         SerialDBG.println("Registered to home network");
         break;
      case(2):
         SerialDBG.println("Not registered, searching for a new operator to register to");
         break;
      case(3):
         SerialDBG.println("Registration denied");
         break;
      case(4):
         SerialDBG.println("Unknown");
         break;
      case(5):
         SerialDBG.println("Registered roaming");
         break;
      default:
         SerialDBG.println("Unknown");
         break;
   }

   // if this is an event with extended info show it for reference purposes
   if(stat==1 || stat==5) {
      SerialDBG.print("Cell Info: Location area code ");
      SerialDBG.print(lac);
      SerialDBG.print(",Cell ID ");
      SerialDBG.println(cellID);
   }  

   return;
}


// takes the CSQ payload and prints out sensible figures
void CSQusSIM800L(char *payload){
   int rssi = 0;
   int ber = 0;
   int percent = 0;

   sscanf(payload, "%d,%d", &rssi, &ber);
   if(rssi == 99) {
      percent = 0;
   } else {
      rssi+=1;
      percent = (int) (100.0/32 * rssi);
   }

   SerialDBG.print("Signal: rssi=");
   SerialDBG.print(percent);
   SerialDBG.print("%, ber=");
   if(ber == 99) {
      SerialDBG.println("unknown");   
   } else {
      SerialDBG.println(ber);
   }    

   return;
}


// takes the CMTI payload and prints out sensible figures
void CMTIusSIM800L(char *payload){
   char store[4];
   int index;

   sscanf(payload, "\"%2s\",%d", store, &index);

   SerialDBG.print("Received SMS stored in: ");
   SerialDBG.print(store);
   SerialDBG.print(", index ");
   SerialDBG.println(index);

   return;
}


// takes a receivedSMS message via +CMT and forwards it over MQTT
// SIM800L>+CMT: "+447740704448","","19/11/18,20:57:23+00"<47
// Unrcognized unsol: +CMT:,D740,+CMT: "+447740704448","","19/11/18,20:57:23+00"
// SIM800L>Even more tests<15
void CMTusSIM800L(char *payload) {
   char smsmsg[GSMMAX + 1];
   char mqttmsg[MQTT_MAX_PACKET_SIZE + 1];
   int smslength = 0;
   int topiclength = 0;
   int hdrlength = 0;

   // get the message content
   FrmSIM800L(smsmsg);

   smslength = strlen(smsmsg);
   topiclength = strlen(OUTTOPIC);

   // copy the payload to a local string so we can modify it
   strcpy(mqttmsg, payload);

   // trim up the header removing unwanted characters and adding a final delimiter
   strim( mqttmsg );

   // calculate the new header length to check mqtt total message size constraints
   hdrlength = strlen(payload);  
    
   // ensure total MQTT message length is not more than pubsubclient can cope with
   // if it is truncate the message string by overwriting a null
   // at a char position that makes the eventual construct the correct length
   if((topiclength + hdrlength + smslength) > MQTT_MAX_PACKET_SIZE) {
      smslength =  MQTT_MAX_PACKET_SIZE - topiclength - hdrlength;
      smsmsg[smslength - 1] = 0x00;
   }

   // add message to the header info to send as a single mqtt message
   strcat(mqttmsg,smsmsg); 

   // flash the LED as we forward the SMS message via MQTT OUTTOPIC
   digitalWrite(BUILTIN_LED, HIGH);
   MQTTtxmsq(mqttmsg);
   digitalWrite(BUILTIN_LED, LOW);
   
   return;
}

// takes a recieved time zone and sync message from the network and dumps it
// SIM800L>*PSUTTZ: 2020,1,13,13,56,17,"+0",0<34
// Unrcognized unsol: *PSUTTZ:,6F40,*PSUTTZ: 2020,1,13,13,56,17,"+0",0
// SIM800L>DST: 0<6
void PSUTZusSIM800L(char *payload) {
   char dstmsg[GSMMAX + 1];

   //dump the DST message which follows
   FrmSIM800L(dstmsg);

   // set a flag so we know +CCLK now returns a valid time stamp
    valid_tstamp = 1;

  return;
}
