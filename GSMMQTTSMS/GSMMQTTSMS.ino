/*
 *  A MQTT to SMS gateway using the TGO T-Call module with SIM800L and ESP32 on board
 *  Debug output serial stream on Serial
 *  SIM800L GSM Modem on Serial1
 *  Main UART used for serial debug linkg and second serial link to modem so that we cans till upload code
 */
#include "GSMMQTTSMS.h"


/* Globals */
WiFiClient espClient;
PubSubClient MQTTclient(espClient);
HardwareSerial SerialGSM(1);
long lastMsg = 0;
char msg[MQTT_MAX_PACKET_SIZE];
int value = 0;
int valid_tstamp = 0;


void setup() {
   String modemretstring;

   uint32_t ideSize = ESP.getFlashChipSize();
   FlashMode_t ideMode = ESP.getFlashChipMode();

   pinMode(BUILTIN_LED, OUTPUT);
   digitalWrite(BUILTIN_LED, LOW);

   // setup the serial connections to how we want it  
   SerialDBG.begin(HOST_BAUD_RATE);
   SerialGSM.begin(SIM800L_BAUD_RATE, SERIAL_8N1, SIM800L_RX, SIM800L_TX);

   // clear the screen on ansi compliant terminals
   SerialDBG.printf("\033[2J\033[1;1H"); //ANSI clear screen escape code

   // print some useful ESP  info
   SerialDBG.println("Booting");
   SerialDBG.println(" ");
   SerialDBG.printf("Flash ide  size: %u", ideSize);
   SerialDBG.println(" ");   
   SerialDBG.printf("Flash ide speed: %u", ESP.getFlashChipSpeed()); 
   SerialDBG.println(" ");
   SerialDBG.printf("Flash ide mode:  %s", (ideMode == FM_QIO ? "QIO" : ideMode == FM_QOUT ? "QOUT" : ideMode == FM_DIO ? "DIO" : ideMode == FM_DOUT ? "DOUT" : "UNKNOWN"));
   SerialDBG.println(" ");

   // setup wifi connection to prefered NoT
   SerialDBG.println("Initialising WiFi connectivity");
   SetupWifi();

   // setup the mqtt connection to prefered broker
   SerialDBG.println("Initialising the MQTT client");
   MQTTsetup();

   // setup the SIM800L GSM module
   SerialDBG.println("Initialising SIM800L GSM Module");
   SetupSIM800L();

   // setup all finished
   SerialDBG.println("Finished setup");

   return;
}


void loop() {
   // check conenction to mqtt broker 
   if (!MQTTclient.connected()) {
      MQTTreconnect();
   }

   // run background mqtt stuff
   MQTTclient.loop();

   // check GSM modem for actionable events
   HandleSIM800L();

   return;
}
