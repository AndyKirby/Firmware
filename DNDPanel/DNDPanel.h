// definitions for these defaults
// usualy set in secrets.h which is git ignored
// create your own secrets.h or edit these here

// includes
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <PubSubClient.h>
//#include <NeoPixelAnimator.h>
#include <NeoPixelBus.h>
//#include <NeoPixelBrightnessBus.h>
//#include <NeoPixelSegmentBus.h>
#include <EasyButton.h>
#include "secrets.h"      // create secrets.h yourself it is git ignored


// defines
#define colorSaturation 128

// wifi network
#ifndef STASSID
   #define STASSID "your-ssid"
   #define STAPSK  "your-password"
#endif

//mqtt server details
#ifndef MQTTSRV
   #define MQTTUSR "your-mqtt-user"
   #define MQTTPWD "your-mqtt-password"
   #define MQTTSRV "your-mqtt-broker"
#endif

//ota password
// definitions for the OTA password
#ifndef OTAPWD
   #define OTAPWD "your-ota-password"
#endif

// mqtt topic prefix definitions
#ifndef INTOPIC
   #define INTOPIC "Your/intopic/prefix/rtrrebooter"
   #define OUTTOPIC "Your/outtopic/prefix/rtrrebooter"
   #define LWTTOPIC "Your/lwt/prefix/rtrrebooter"
#endif

#define WS2812_PIN 2  // D4 the GPIO pin that is connected to the string of ws2812's
#define LED_PIN 16    // D0 the onboard LED
#define BUTTON_PIN 0  // D3 gpio pin for flash button
