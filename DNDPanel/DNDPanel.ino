
/*
  MQTT controled Do-Not-Disturb/Pester-At-Will panel

  This sketch does basic MQTT to turn a relay on/off.

*/
#include "DNDPanel.h"


// Global variables
// Update these with values suitable for your network via
// DNDPanel.h or better still roll your own secrets.h
const char* ssid = STASSID;
const char* ssid_pwd = STAPSK;
const char* mqtt_usr = MQTTUSR;
const char* mqtt_pwd = MQTTPWD;
const char* mqtt_broker = MQTTSRV;
const char* ota_pwd = OTAPWD;
const char* topic_in = INTOPIC;
const char* topic_out = OUTTOPIC;
const char* topic_lwt = LWTTOPIC;
long lastMsg = 0;
char msg[50];
String clientId = "ESP8266-";
unsigned long lastmillis = 0;
int status = 0;

const uint16_t PixelCount = 6;      // this example assumes 4 pixels, making it smaller will cause a failure
const uint8_t PixelPin = WS2812_PIN;  // make sure to set this to the correct pin, ignored for Esp8266


// Global objects
WiFiClient espClient;
PubSubClient client(espClient);
NeoPixelBus<NeoGrbFeature,NeoEsp8266Uart1800KbpsMethod> strip(PixelCount, PixelPin);
RgbColor red(colorSaturation, 0, 0);
RgbColor green(0, colorSaturation, 0);
RgbColor blue(0, 0, colorSaturation);
RgbColor white(colorSaturation);
RgbColor black(0);
EasyButton button(BUTTON_PIN);


// wifi setup code
void setup_wifi() {
   int timeout = 0;
  
   delay(10);
   Serial.println(F("*** Wifi ***"));
   Serial.print(F("SSID: "));
   Serial.println(ssid);
   Serial.print(F("Key: "));
   Serial.println(ssid_pwd);

   WiFi.mode(WIFI_STA);
   WiFi.begin(ssid, ssid_pwd);
   Serial.print(F("Waiting for connection"));
   while(WiFi.status() != WL_CONNECTED) {
      delay(1000);
      Serial.print(".");
      if(timeout++ > 30) {
         Serial.println();
         Serial.print(F("Wifi timed out! rebooting."));
         ESP.restart();
      }
   }
   Serial.println();

   randomSeed(micros());

   Serial.print(F("IP address: "));
   Serial.println(WiFi.localIP());
   Serial.print(F("MAC address: "));
   Serial.println(WiFi.macAddress());

}

void setup_mqtt() {
   Serial.println(F("*** MQTT ***"));
   clientId += String(ESP.getChipId(), HEX);
   Serial.print(F("client ID: "));
   Serial.println(clientId);
   Serial.print(F("mqtt_usr: "));
   Serial.println(mqtt_usr);
   Serial.print(F("Password: "));
   Serial.println(mqtt_pwd);
   Serial.print(F("Broker: "));
   Serial.println(mqtt_broker);  
   client.setServer(mqtt_broker, 1883);
   client.setCallback(callback);
   mqtt_reconnect();
     
}


// button pressed do somethign with it
void onPressed() {
   Serial.println(F("Button pressed"));
   if(status) {
      lightPAW();
   } else {
      lightDND();
   }

   return;
}


// mqtt message arrived do something with it
void callback(char* topic, byte* payload, unsigned int length) {
   char newtopic[60];
   unsigned int i;
   
   Serial.print(F("Message arrived ["));
   Serial.print(topic);
   Serial.print(F("] "));
   for (i = 0; i < length; i++) {
     Serial.print((char)payload[i]);
   }
   Serial.println();

   // 0 light up pester at will
   // 1 light up do not disturb
   switch((char)payload[0]){
      case '0':     
         lightPAW();
         break;
      case '1':
         lightDND();     
         break;
   }
}


// mqtt client connect/re-connect code
void mqtt_reconnect() {
   int retries;

   // dont bother trying if we are already connected
   if(client.connected()){
      return;
   }

   // to get this far we are not conencted so try to connect
   for(retries = 0; retries < 5; retries++) {
      Serial.print(F("Attempting MQTT connection..."));     
      if (client.connect(clientId.c_str(),mqtt_usr,mqtt_pwd,topic_lwt,0,0,"dead")) {
         Serial.println(F("connected"));
         client.publish(topic_lwt, "alive");
         client.subscribe(topic_in);
         break;
      } else {
         Serial.print(F("failed, rc="));
         Serial.println(client.state());
      }
   }

   return;
}


// actual setup code
void setup() {
   Serial.begin(115200);
   Serial.printf("\n\n");
   Serial.println(F("*** Starting Setup ***"));

   // setup and light the onboard LED
   
   // initialise and test neopixels
   stripsetup();

   // report device info
   espdetails();

   // start wifi
   setup_wifi();

   // start mqtt client
   setup_mqtt();
   
   Serial.println(F("*** Setup complete ***"));

   // initialise millis
   lastmillis = millis();

   // setup built in led pin
   pinMode(LED_PIN, OUTPUT);
   digitalWrite(LED_PIN, HIGH);

   //setup button
   button.begin();
   button.onPressed(onPressed);

   // light the pester at will panel
   lightPAW();

   return;
}


// main program loop
void loop() {
   int timeout = 0;
   String number;
   
   // service the wifi connection
   if(WiFi.status() != WL_CONNECTED) {
      Serial.println(F("Wifi connection failed attempting reconnect!"));
      WiFi.reconnect();
      while(WiFi.status() != WL_CONNECTED) {
         delay(1000);
         Serial.print(".");
         if(timeout++ > 30) {
            Serial.println();
            Serial.print(F("Wifi reconnect timed out! rebooting."));
            WiFi.printDiag(Serial);
            ESP.restart();
         }
      }
      Serial.println(F("Wifi reconnected"));
   }
  
   // service mqtt client and callbacks
   if (!client.connected()) {
      Serial.println(F("MQTT connection failed attempting reconnect!"));
      mqtt_reconnect();
   } else {
      client.loop();
   }

   // read the flash button
   button.read();

   // once a second blink the on board LED and send the status via MQTT
   if( (millis() - lastmillis) > 1000 ) {
      //toggle the onboard LED
      digitalWrite(LED_PIN , !digitalRead(LED_PIN));
      lastmillis = millis();
      number = String(status);
      mqttsend(topic_out, number.c_str());
   }

   return;
}


// *** utility functions *** //

// mqtt publish only if there is a current connection
// otherwise there is no point
void mqttsend(const char* topic, const char* message){
  if (client.connected()) {
     client.publish(topic, message);
  }
  return;
}


// printout some usefull diagnostic info just in case
void espdetails(){
   FlashMode_t ideMode = ESP.getFlashChipMode();

   Serial.println(F("*** Last Reset ***"));
   Serial.println(ESP.getResetReason());
   Serial.println(ESP.getResetInfo());

   Serial.println(F("*** Device ***"));
   Serial.print(F("ESP Chip ID: "));
   Serial.printf("%08X", ESP.getChipId());
   Serial.println();
   Serial.print(F("Core Version: "));
   Serial.println(ESP.getCoreVersion());
   Serial.print(F("CPU Freq: "));
   Serial.printf("%d Mhz", ESP.getCpuFreqMHz());
   Serial.println();  
   
   Serial.println(F("*** Flash ***"));
   Serial.printf("Flash real id: %08X", ESP.getFlashChipId());
   Serial.println();
   Serial.print(F("Flash real size: "));
   Serial.println(ESP.getFlashChipRealSize());
   Serial.print(F("Flash ide  size: "));
   Serial.println(ESP.getFlashChipSize());   
   Serial.print(F("Flash ide speed: ")); 
   Serial.println(ESP.getFlashChipSpeed());
   Serial.printf("Flash ide mode:  %s", (ideMode == FM_QIO ? "QIO" : ideMode == FM_QOUT ? "QOUT" : ideMode == FM_DIO ? "DIO" : ideMode == FM_DOUT ? "DOUT" : "UNKNOWN"));
   Serial.println();
   if(ESP.checkFlashConfig(true)) {
      Serial.println(F("Flash Chip configuration ok"));
   } else {
      Serial.println(F("Flash Chip configuration wrong!"));
   }
   Serial.print(F("Sketch Size: "));
   Serial.println(ESP.getSketchSize());
   Serial.print(F("Sketch Free: "));
   Serial.println(ESP.getFreeSketchSpace());
    
   Serial.println(F("*** Memory ***"));
   Serial.print(F("Current Heap: "));
   Serial.println(ESP.getFreeHeap());

   Serial.println(F("*** Environment ***"));
   Serial.print(F("SDK Version:  "));
   Serial.println(ESP.getSdkVersion());
   Serial.print(F("Boot mode: "));
   Serial.println(ESP.getBootMode());
   Serial.print(F("Boot Version: "));
   Serial.println(ESP.getBootVersion());   
   Serial.print(F("Vcc: "));
   Serial.println(ESP.getVcc());

   return;
}


void stripsetup() {
   Serial.println(F("*** Setup Neopixels ***"));
   Serial.print(F("WS2812 strip on GPIO: "));
   Serial.println(PixelPin);
     
   strip.Begin();
   strip.Show();
   
   Serial.println(F("WS2812 strip: On"));
   strip.SetPixelColor(0, red);
   strip.SetPixelColor(1, red);
   strip.SetPixelColor(2, red);
   strip.SetPixelColor(3, green);
   strip.SetPixelColor(4, green);
   strip.SetPixelColor(5, green);
   strip.Show();
   delay(5000);

   Serial.println(F("WS2812 strip: Off"));  
   strip.SetPixelColor(0, black);
   strip.SetPixelColor(1, black);
   strip.SetPixelColor(2, black);
   strip.SetPixelColor(3, black);
   strip.SetPixelColor(4, black);
   strip.SetPixelColor(5, black);
   strip.Show();

   Serial.println(F("WS2812 strip test: Complete"));

   return;
}


void lightDND() {
   Serial.println(F("lighting DND"));
  
   strip.SetPixelColor(0, red);
   strip.SetPixelColor(1, red);
   strip.SetPixelColor(2, red);
   strip.SetPixelColor(3, black);
   strip.SetPixelColor(4, black);
   strip.SetPixelColor(5, black);
   strip.Show();

   mqttsend(topic_out, "1");
   status = 1;

   return;
}


void lightPAW() {
   Serial.println(F("lighting PAW"));
  
   strip.SetPixelColor(0, black);
   strip.SetPixelColor(1, black);
   strip.SetPixelColor(2, black);
   strip.SetPixelColor(3, green);
   strip.SetPixelColor(4, green);
   strip.SetPixelColor(5, green);
   strip.Show();

   mqttsend(topic_out, "0");
   status = 0;

   return;
}
