# A Prototype Inertial Logger

![Hardware Photo](InLog.jpg)

A simple inertial logger usuing off the shelf components for experimenting with inertial navigation and tracking. Not for any end purpose in particular but could be used for track logging anywhere including where GPS is not abvailable. Example applications are underwater navigation, high altitude ballooning and cave maping.

## Teensy Pinout, Teensy digital pin numbering

### SD CARD, SPI Mode
* Pin 11, MOSI
* Pin 12, MISO (10K  pullup to 3v3)
* Pin 14, SCK
* Pin 20, CS

### GY-87 10DoF Breakout
* Pin 19, SCL0 (4k7 pullup to 3v3)
* Pin 18, SDA0 (4k7 pullup to 3v3)
* Pin 17, FSYNC
* Pin 16, INTA
* Pin 15, DRDY

### PowerBoost 500C, Lipo supply and charger
* Pin 3, Low Battery
* Pin 4, PSU Enable

## Notes
1. IMU is chinese GY-87 10DoF board (BMP180, MPU-6050 & HMC5883L)
2. CPU is a Teensy 3.1, with RTC crystal and battery backup
3. PSU is an Adafruit PowerBoost 500 C (With lipo chrager) and lipo 2.5Ah battery VUSB broken out from Teensy
4. SD Card powered from 5 to 3v3 regulator on GY-87 board
5. Sources for intial testing and info on the GY-87 all shamlesly pillaged from: http://www.control.aau.dk/~jdn/edu/doc/arduino/gy80gy87/ Bigups and credit where it is due, an excellent site and well worth the visit/read.
6. Sources for initial testing and info on the Teensy 3.1: https://www.pjrc.com/teensy/teensy31.html
7. Sources for initial testing and info on the Lipo PSU/charger from: http://adafrui.it/1944

![Close up photo](prototype.jpg)
