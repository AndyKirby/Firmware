/* Master Header file for Inertial Logger project */
/* data for current sea level pressure http://en.allmetsat.com/metar-taf/united-kingdom-ireland.php?icao=EGCN */


/* Includes */
   // NB cannot put includes must be in ino file or arduino ide chokes on it

/* Defines */
   //Scheduler stuff
   #define Interval 1000UL // Number of microseconds that each scheduler slot should take to complete
   #define NumSlots 1000 // Number of scheduler slots NumSlots * Interval should equal 1 second as this is our logging frequency

   // BMP180 Stuff
   #define Sea_Level         102150L // Pressure at ideal mean sea level in pascals default 101325L
   #define BMP180_ADDR       0x77    // BMP180 I2C Address
   #define BMP180_REG_XLSB   0xF8    // ADC out rexister xlb
   #define BMP180_REG_LSB    0XF7    // ADC out register lsb
   #define BMP180_REG_MSB    0xF6    // ADC out register msb
   #define BMP180_REG_CTRL   0xF4    // Measurement control register
   #define BMP180_REG_RST    0xE0    // Soft Reset write 0xB6 for pwr on reset
   #define BMP180_REG_ID     0xD0    // Chip ID is 0x55, read to check BMP180 present and ok
   #define BMP180_REG_CAL_AC1  0xAA  // Factory calibration value
   #define BMP180_REG_CAL_AC2  0xAC  // Factory calibration value
   #define BMP180_REG_CAL_AC3  0xAE  // Factory calibration value
   #define BMP180_REG_CAL_AC4  0xB0  // Factory calibration value
   #define BMP180_REG_CAL_AC5  0xB2  // Factory calibration value
   #define BMP180_REG_CAL_AC6  0xB4  // Factory calibration value
   #define BMP180_REG_CAL_B1   0xB6  // Factory calibration value
   #define BMP180_REG_CAL_B2   0xB8  // Factory calibration value
   #define BMP180_REG_CAL_MB   0xBA  // Factory calibration value
   #define BMP180_REG_CAL_MC   0xBC  // Factory calibration value
   #define BMP180_REG_CAL_MD   0xBE  // Factory calibration value
   #define BMP180_OSS_ULOW     0x00  // Oversample setting ultra low power 4.5mS
   #define BMP180_OSS_STD      0x01  // Oversample setting standard 7.5mS
   #define BMP180_OSS_HIGH     0x02  // Oversample setting high resolution 13.5mS
   #define BMP180_OSS_UHIGH    0x03  // Oversample setting ultra high resolution 25.5mS
   #define BMP180_STRT_TEMP    0x2E  // Start temperature conversion control register value
   #define BMP180_STRT_PRES    0x34  // Start pressure conversion control register value
   

/* Global Constants */


/* Global Variables and objects*/

   //scheduler stuff
   unsigned long nextturn; // variable that holds the terminal millis count for a scheduler slot
   int slot = 1;

   // BMP180 stuff
   float temperature; // temperature corrected in degrees C
   float pressure; // pressure corected in pascals
   float altitude; // altitude in Meters above mean sea level  
   // Calibration variables, may have to pack this into a struct or some such if space gets tight.
   short ac1;
   short ac2;
   short ac3;
   unsigned short ac4;
   unsigned short ac5;
   unsigned short ac6;
   short b1;
   short b2;
   short mb;
   short mc;
   short md;
   long b5; // b5 is calculated in bmp085GetTemperature(), but used in bmp085GetPressure(), so Temperature() must be called before Pressure()
 

   // MPU6050 stuff
   MPU6050 mpu;
   Vector rawAccel;
   Vector normAccel;
   Vector rawGyro;
   Vector normGyro;


   // HMC5883L stuff
   hmc5883l compass;
   int error_hmc;


/* prototypes */

   // BMP180
   void bmp180Calibration();
   long bmp180ReadUT();
   long bmp180ReadUP(char OSS);
   float bmp180GetTemperature(long ut);
   long bmp180GetPressure(unsigned long up, char OSS);
   long bmp180Pressure();
   float bmp180Temp();
   float bmp180PascalToMeter(long pressure, long seaLevel);
   float calcAltitude(long pressure, long seaLevel);

   // I2C Routines based on wiring.h
   void i2cRegWr8(int deviceAddress, char address, char val);
   char i2cRegRd8(int deviceAddress, char address);
   unsigned short i2cRegRd16(int deviceAddress, char address);



