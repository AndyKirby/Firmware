/* Includes */
#include <Arduino.h>
#include <math.h>
#include <Wire.h>
#include <SPI.h>
#include <SD_t3.h>
#include <SD.h>
#include <Time.h>
#include "MPU6050.h"
#include "hmc5883l.h"

/* Inertial Logger logs the pressure temp altitude 3D accel, 3D gyro, 3D magnetometer as values deltas and integral deltas
   Note: This code is wirten for the Teensy 3.1 and is therefore 32 bit data type compliant
   Author: Andy Kirby, andy@kirbaynd.co.uk, www.kirbyand.co.uk
   License: See license file where this code came from, https://github.com/AndyKirby/Firmware
   CSV log Format: timestamp,temperature C ,pressure atm,alt M,,,,,,,,,,,
   BMP180 code borrowed heavily from BMP085 code:-
      http://ilabbali.com/code/Arduino_BMP085.cpp
      http://bildr.org/2011/06/bmp085-arduino/
*/

#include "InertialLogger.h"

// The usual arduino setup routine 
void setup() {
   Serial.begin(115200);

   // initialise i2c
   Wire.begin();

   // initialise the mpu6050, 2000 degrees pr second max and +/- 16 g max
   mpu.begin(MPU6050_SCALE_2000DPS, MPU6050_RANGE_16G);
   mpu.setI2CBypassEnabled(true); // set bypass mode
   
   // now we can reach hmc5883l
   compass = hmc5883l(); // Construct a new HMC5883 compass.
   Serial.println("Setting scale to +/- 1.3 Ga");
   error_hmc = compass.SetScale(1.3); // Set the scale of the compass.
   error_hmc = compass.SetMeasurementMode(Measurement_Continuous); // Set the measurement mode to Continuous

   // Get the calibration values from the BMP180
   bmp180Calibration();

   // Calculate when the first scheduler slots interval should finish ie
   // interval microseconds from now
   nextturn = micros() + Interval;

   return;
}


//The usual arduino main loop
void loop() {
   

   // Scheduler state machine, slots are Interval micro seconds long
   // Any slot needing more than on Interval should recalculate nextturn before carryinmg out it's tasks
   switch(slot++) {
      case(1):
         // todo add branch to ui on keys, if poss keep on logging data to sd card ?? etc
         //     or do we need to make this a packet based thing and write a python client for ui or some such...
         // todo add ui to set sea level pressure
         // todo add ui to set time n date
         break;
      case(10):
         // Command BMP180 to start a temperature conversion, takes 5mS
         i2cRegWr8(BMP180_ADDR, BMP180_REG_CTRL, BMP180_STRT_TEMP);
         break;
      case(15):
         // Read temperature then compensate and translate to Deg C 
         temperature = bmp180GetTemperature((long) i2cRegRd16(BMP180_ADDR, BMP180_REG_MSB));
         // Command BMP180 to start pressure conversion ultrahigh resolution takes 25.5 mS
         i2cRegWr8(BMP180_ADDR, BMP180_REG_CTRL, (BMP180_STRT_PRES + (BMP180_OSS_UHIGH<<6)));
         break;
      case(41):
         // Read pressure then compensate and translate to hPa (mBar)
         pressure = bmp180GetPressure(bmp180ReadUP(BMP180_OSS_UHIGH), BMP180_OSS_UHIGH);
         // Convert pressure to altitude in Meters above mean sea level
         altitude = calcAltitude(pressure, Sea_Level);      
         break;
      case(NumSlots - 1):
         //Log to USB
         // read and print time_t timestamp from teensy rtc
         Serial.print(Teensy3Clock.get());
         Serial.print(",");
         // compensated temperature
         Serial.print(temperature);
         Serial.print(",");
         // compensated pressure in hPa or mBar
         Serial.print(pressure/100);
         Serial.print(",");
         // calculated altitude in meters                           
         Serial.println(altitude);         
      case(NumSlots):
         slot = 1;
         break;
      default:
         // As this is a sparse scheduler we need this for the slots that are not filled
         break;
   }

   if( nextturn - micros() > Interval ) {
      // tell somone that the timing is gettign screwed up
      // idealy this should never happen, coding intervention is required
      // slots needing more than one interval to complete their tasks should extend nextrun
      Serial.print(slot);
      Serial.println(": overrun");
   }

   while( nextturn - micros() < Interval ) {
      // waiting for end of slot
      // put sleeps etc in here   
   }
  
   // Calculate end of next cycle time  
   nextturn += Interval;

   return;
}

/* BMP180 routines */
 
// Copies all of the bmp180's calibration values into global variables
// Calibration values are required to calculate temp and pressure
// This function should be called at the beginning of the program
void bmp180Calibration()
{
  ac1 = (short) i2cRegRd16(BMP180_ADDR,BMP180_REG_CAL_AC1);
  ac2 = (short) i2cRegRd16(BMP180_ADDR,BMP180_REG_CAL_AC2);
  ac3 = (short) i2cRegRd16(BMP180_ADDR,BMP180_REG_CAL_AC3);
  ac4 = (unsigned short) i2cRegRd16(BMP180_ADDR,BMP180_REG_CAL_AC4);
  ac5 = (unsigned short) i2cRegRd16(BMP180_ADDR,BMP180_REG_CAL_AC5);
  ac6 = (unsigned short) i2cRegRd16(BMP180_ADDR,BMP180_REG_CAL_AC6);
  b1 = (short) i2cRegRd16(BMP180_ADDR,BMP180_REG_CAL_B1);
  b2 = (short) i2cRegRd16(BMP180_ADDR,BMP180_REG_CAL_B2);
  mb = (short) i2cRegRd16(BMP180_ADDR,BMP180_REG_CAL_MB);
  mc = (short) i2cRegRd16(BMP180_ADDR,BMP180_REG_CAL_MC);
  md = (short) i2cRegRd16(BMP180_ADDR,BMP180_REG_CAL_MD);

  return;
}


// Calculate temperature in deg C
float bmp180GetTemperature(long ut){
  long x1;
  long x2;
  long t;
  float temp;

  x1 = (((long)ut - (long)ac6)*(long)ac5) >> 15;
  x2 = ((long)mc << 11)/(x1 + md);
  b5 = x1 + x2;
  t = ((b5 + 8)>>4);
  temp = t/10.0;

  return temp;
}


// Calculate pressure given up
// calibration values must be known
// b5 is also required so bmp085GetTemperature(...) must be called first.
// Value returned will be pressure in units of Pa.
long bmp180GetPressure(long up, char OSS){
  long x1, x2, x3, b3, b6, p;
  unsigned long b4, b7;

  // Calculate b6
  b6 = b5 - 4000;
  
  // Calculate b3
  x1 = (b2 * (b6 * b6)>>12)>>11;
  x2 = (ac2 * b6)>>11;
  x3 = x1 + x2;
  b3 = (((((long)ac1)*4 + x3)<<OSS) + 2)>>2;

  // Calculate b4
  x1 = (ac3 * b6)>>13;
  x2 = (b1 * ((b6 * b6)>>12))>>16;
  x3 = ((x1 + x2) + 2)>>2;
  b4 = (ac4 * (unsigned long)(x3 + 32768))>>15;

  // Calculate b7
  b7 = ((unsigned long)(up - b3) * (50000>>OSS));
  
  if (b7 < 0x80000000) {
    p = (b7<<1)/b4;
  } else {
    p = (b7/b4)<<1;
  }

  x1 = (p>>8) * (p>>8);
  x1 = (x1 * 3038)>>16;
  x2 = (-7357 * p)>>16;
  p = p + ((x1 + x2 + 3791)>>4);

  return p;
}


// Read the uncompensated pressure from bmp180
long bmp180ReadUP(char OSS){
  unsigned long msb, lsb, xlsb;

  // Read register 0xF6 (MSB), 0xF7 (LSB), and 0xF8 (XLSB)
  msb = (long) i2cRegRd8(BMP180_ADDR,0xF6);
  lsb = (long) i2cRegRd8(BMP180_ADDR,0xF7);
  xlsb = (long) i2cRegRd8(BMP180_ADDR,0xF8);

  return ((msb << 16) | (lsb << 8) | xlsb) >> (8-OSS);
}


// Calculate altitude from current sealevel pressure and measured pressure
float calcAltitude(long pressure,long seaLevel){

  float A = (1.0 * pressure)/seaLevel;
  float B = 1.0/5.25588;
  float C = pow(A,B);
  C = 1.0 - C;
  C = C /0.0000225577;

  return C;
}


/* Common I2C routines based on arduino wiring library calls */

// Write a val of 1 byte to address on deviceAddress
void i2cRegWr8(int deviceAddress, char address, char val) {
  Wire.beginTransmission(deviceAddress); // start transmission to device 
  Wire.write(address);                   // send register address
  Wire.write(val);                       // send value to write
  Wire.endTransmission();                // end transmission

  return;
}


// read 1 byte from address on deviceAddress
char i2cRegRd8(int deviceAddress, char address) {
  Wire.beginTransmission(deviceAddress); // start transmission to device 
  Wire.write(address);                   // send register address
  Wire.endTransmission();                // end transmission

  Wire.requestFrom(deviceAddress, 1);    // read one byte

  while(!Wire.available()) {             // wait for it to arrive
    // waiting
  }

  return(Wire.read());                   // return the read value
}


// read 2 byte word from address on deviceAddress
unsigned short i2cRegRd16(int deviceAddress, char address) {
  unsigned short msb, lsb;

  Wire.beginTransmission(deviceAddress); // start transmission to device 
  Wire.write(address);                   // send register address
  Wire.endTransmission();                // end transmission

  Wire.requestFrom(deviceAddress, 2);    // read two bytes
  
  while(Wire.available()<2) {            // wait for them to arrive
     // waiting
  }
  
  msb = Wire.read();
  lsb = Wire.read();

  return(msb<<8 | lsb);                  // return the read value
}


