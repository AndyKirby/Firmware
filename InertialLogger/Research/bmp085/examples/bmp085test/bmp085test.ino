#include <Wire.h>
#include <bmp085.h>

#define PRES_0M 101325.0

void setup()
{
  Serial.begin(9600);

  bmp085_init();
}

float temp,atm,alt;
long pres;
 
#define LOOPTIME 200
unsigned long t1,t3;
void loop()
{
 //  bmp085_init();
  temp = bmp085Temp(); 
  pres = bmp085Pressure(); 

  atm = pres / PRES_0M; // "standard atmosphere"
  alt = bmp085PascalToMeter(pres,PRES_0M);  
  Serial.print(temp);
  Serial.print(" C ");

  Serial.print(pres);
  Serial.print(" Pa ");
 
  Serial.print(atm);
  Serial.print(" (rel atm) ");

  Serial.print(alt);
  Serial.println( " m ");
 
}







 
