#include <bmp085.h>

#include <Wire.h>

#define ONEATMOSPHERE 102510.0

#define AVERAGELENGTH 10
float average[AVERAGELENGTH];
int i;

void setup()
{
  Serial.begin(9600);
  bmp085_init();
  i = 0;
  while (i < AVERAGELENGTH)
  {
    average[i] = 0.0; // best guess
    i = i+1;
  }
}

float calcAverage()
{
  int k;
  float sum;
  k = 0;
  while (k < AVERAGELENGTH)
  {
    sum = sum + average[k];
    k = k+1;
  }
  return sum / AVERAGELENGTH;
}

float temp;
long presPascal,presAtm;
float height;


void loop()
{
  temp = bmp085Temp();
  presPascal = bmp085Pressure();
  presAtm = presPascal / ONEATMOSPHERE;
  height =  bmp085PascalToMeter(presPascal,ONEATMOSPHERE);
  average[i] = height;
  i = i+1;
  if (AVERAGELENGTH <= i)
    i = 0;  // Loop
  average[i] = height;
  Serial.print(height,1);
  Serial.print(" 10 values mean ");
  Serial.print(calcAverage() ,1);
  Serial.println(" m");
  delay(100);
}




