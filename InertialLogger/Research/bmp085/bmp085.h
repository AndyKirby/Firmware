

void bmp085_init();

// do all measurements
void bmp085_measure();

// Stores all of the bmp085's calibration values into global variables
// Calibration values are required to calculate temp and pressure
// This function should be called at the beginning of the program
void bmp085Calibration();

// Calculate temperature in deg C
float bmp085GetTemperature(unsigned int ut);

// Read the uncompensated pressure value
unsigned long bmp085ReadUP();


// Calculate pressure given up
// calibration values must be known
// b5 is also required so bmp085GetTemperature(...) must be called first.
// Value returned will be pressure in units of Pa.
long bmp085GetPressure(unsigned long up);

long bmp085Pressure();
float bmp085Temp();
float bmp085PascalToMeter(long pressure,long seaLevel);


// Read 1 char from the BMP085 at 'address'
char bmp085Read(char address);


// Read 2 chars from the BMP085
// First char will be from 'address'
// Second char will be from 'address'+1
int bmp085ReadInt(char address);


// Read the uncompensated temperature value
unsigned int bmp085ReadUT();

float calcAltitude(long pressure, long seaLevel);



