#include <Wire.h>
#include <MPU6050.h>
#include <hmc5883l.h>
#include <bmp085.h>

// afdjust to the pressure of today at sealevel for correct altitude
#define PRES_0M 101325.0

MPU6050 mpu;
Vector rawAccel, normAccel, rawGyro, normGyro;
hmc5883l compass;

int error;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  // 2000 degrees pr second max and +/- 16 g max
  mpu.begin(MPU6050_SCALE_2000DPS, MPU6050_RANGE_16G);
  mpu.setI2CBypassEnabled(true); // set bypass mode
  // now we can reach hmc5883l
  compass = hmc5883l(); // Construct a new HMC5883 compass.

  Serial.println("Setting scale to +/- 1.3 Ga");
  error = compass.SetScale(1.3); // Set the scale of the compass.
  error = compass.SetMeasurementMode(Measurement_Continuous); // Set the measurement mode to Continuous

  bmp085_init();


}

void doPresAndTemp()
{
  float temp, atm, alt;
  long pres;
  //  bmp085_init();
  temp = bmp085Temp();
  pres = bmp085Pressure();

  atm = pres / PRES_0M; // "standard atmosphere"
  alt = bmp085PascalToMeter(pres, PRES_0M);
  Serial.print("Pres and Temp ");
  Serial.print(temp);
  Serial.print(" C ");

  Serial.print(pres);
  Serial.print(" Pa ");

  Serial.print(atm);
  Serial.print(" (rel atm) ");

  Serial.print(alt);
  Serial.println( " m ");

}

