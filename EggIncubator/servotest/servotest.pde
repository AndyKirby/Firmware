// Servo tester to find limits of servo movment in uS
// Trial and error modify Max and Min SPULSE values until the servo hits the end stops

// Global Constants
#define BUTT_POSITIONER 2    // Digital pin the ducks butt positioner servo is connected to
#define ONE_SECOND 1000      // The number of milliseconds in one second
#define MIN_SPULSE 250      // Minimum pulse length for servo driving in uS
#define MAX_SPULSE 2250      // Maximum pulse length for servo driving in uS
#define SERVO_PERIOD 20      // Perios of a servo cycle in mS
 
// Global vaiables
int ducksbutt_pos = MIN_SPULSE + ((MAX_SPULSE - MIN_SPULSE) / 2); // variable to store the position of the ducks butt, initialise to BDC
unsigned long last_second = 0;        // Last millisecond count when timing one second
unsigned long lastpulse = 0;          // When last servo pulse was issued in mS
int action = 0;
 
void setup() 
{ 
  digitalWrite(BUTT_POSITIONER, LOW); // Preset the servo pin low
  last_second = millis(); // initialise millis count for timing in loop()
} 
 
 
void loop() 
{
  
  
  // as often as possible check and if needed refresh the servo position
  // If the timing is a little iregular it does'nt matter
  do_servo();
  
  // Once a second do something useful
  // If millis count has wraped around re read it, otherwise see if a second has elapsed
  if(millis() < last_second) { 
    last_second = millis();
  } else {  
    if (millis() - last_second >= ONE_SECOND) {
      // reload last_second to mark this cycle
      last_second = millis();
      
      switch (action) {
        case 0 :
          ducksbutt_pos = MIN_SPULSE;
          action++;
          break;
        case 1 :
          ducksbutt_pos = MIN_SPULSE + ((MAX_SPULSE - MIN_SPULSE) / 2);
          action++;
          break;          
        case 2 :
          ducksbutt_pos = MAX_SPULSE;
          action++;
          break;
        default :
          ducksbutt_pos = MIN_SPULSE + ((MAX_SPULSE - MIN_SPULSE) / 2);
          action = 0;
          break;
       }

    

    }
  }
  
}


// Call this as often as possible and it will sort out the servo pulse generation and refresh every 20ms
void do_servo(){
  
  // If we are due to issue another pulse do it
  if (millis() - lastpulse >= SERVO_PERIOD) {

    lastpulse = millis();                // store the time this pulse took place      
    digitalWrite(BUTT_POSITIONER, HIGH); // start the pulse  
    delayMicroseconds(ducksbutt_pos);    // wait for the pulse to be long enough  
    digitalWrite(BUTT_POSITIONER, LOW);  // stop the pulse  

  }  
  
}
