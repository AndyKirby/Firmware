// Duck Egg Incubator Controler Firmware
// by aka47 <andy@kirbyand.co.uk, http://www.kirbyand.co.uk/>
// An incubator artificialy simulates the presence of a ducks butt on the eggs to be hatched.
// It Warms them to the correct temperature, and regularly moves them, a capable incubator should do similar

// Global Constants
#define BUTT_TEMP 0              // Analogue pin ducks butt temperature sensor is connected to
#define BUTT_HEATER 11            // PWM Pin duck butt heater is connected to
#define TARGET_TEMP 355          // Tweak this value to one calibrated against a good thermometer target 37.5 C
#define HEATER_OFF 0             // Off value to ensure heater starts up turned off
#define BUTT_POSITIONER 2        // Digital pin the ducks butt positioner servo is connected to
#define FORWARD 1                // FORWARD direction of movement of the ducks butt
#define BACKWARD 0               // BACKWARD direction of movment of the ducks butt
#define DEBUG_PIN 13             // the led pin to blink
#define ONE_SECOND 1000          // The number of milliseconds in one second
#define MIN_SPULSE 1000          // Minimum pulse length for servo driving in uS
#define MAX_SPULSE 2000          // Maximum pulse length for servo driving in uS
#define SERVO_PERIOD 20          // Period of a servo cycle in mS
#define TEMP_PERIOD 400          // Period of a Temperature Poll
#define GAIN_PROP 65              // PID Proportional Gain Coeficient
#define GAIN_INTG 1              // PID Integral Gain Coeficient
#define CAPP_INTG 65            // PID Positive Integral Cap to prevent wind up
#define CAPN_INTG -65           // PID Negative Integral Cap to prevent wind up 
#define GAIN_DIFF 1              // PID Differential Gain Coeficient
#define PWM_MAX 255              // maximum value that analogwrite can take
#define PWM_MIN 0                // minimum value that analogwrite can take
 
// Global vaiables
int debug_state = LOW;           // state of debug pin
int ducksbutt_pos = MIN_SPULSE + ((MAX_SPULSE - MIN_SPULSE) / 2); // variable to store the position of the ducks butt, initialise to BDC
int ducksbutt_temp = 0;          // variable to store the temperature of the ducks butt
int dbhv1;                       // moving average container for ducks but heater signal conditioning
int dbhv2;                       // moving average container for ducks but heater signal conditioning
int dbhv3;                       // moving average container for ducks but heater signal conditioning
int dbhv4;                       // moving average container for ducks but heater signal conditioning
int dbhv5;                       // moving average container for ducks but heater signal conditioning
int ducksbutt_setpoint = TARGET_TEMP; //variable to store the set point temperature of the ducks butt
int ducksbutt_dir = FORWARD;     // The direction of movment of the ducks butt
int pwr_level = HEATER_OFF;      // The power level that the ducks butt heater is currently driven at
char buff[80];                   //Character buffer for assembling printing lines.
unsigned long last_second = 0;   // Last millisecond count when timing one second
unsigned long lastpulse = 0;      // When last servo pulse was issued and temperature read in mS
unsigned long lastpoll = 0;
int PIDerror = 0;                // Heater PID loop error value this time around
int PIDlasterror = 0;            // Heater PID loop error value last time around
int PIDintgerror = 0;            // Heater PID loop cumulative error, ie integral term
 
void setup() 
{ 
  Serial.begin(19200);           // Setup serial output on host link
  Serial.println("Duck Egg Incubator Controler Version 0.1"); // Print somethign to show we are alive
  Serial.println("Servo,Dir,Set,Temp,Error,Pwr"); // A help banner to remind a reader of the field values
  pinMode(BUTT_POSITIONER, OUTPUT); // Set the servo driving pin to be and output
  pinMode(BUTT_HEATER, OUTPUT);  // Set the heater driving pin to be an output
  digitalWrite(BUTT_POSITIONER, LOW); // Preset the servo pin low
  analogWrite(BUTT_HEATER, HEATER_OFF); // Force the heater to off
  ducksbutt_temp = analogRead(BUTT_TEMP); // Do a first read of the ducks butt temperature
  dbhv1 = ducksbutt_temp;         // initialise the moving average
  dbhv2 = ducksbutt_temp;         // initialise the moving average
  dbhv3 = ducksbutt_temp;         // initialise the moving average
  dbhv4 = ducksbutt_temp;         // initialise the moving average
  dbhv5 = ducksbutt_temp;         // initialise the moving average
  lastpoll = millis();            // Initialise millis count for temperature read  timing in loop()
  lastpulse = millis();           // Initialise millis count for servo pulse timing in loop()
  last_second = millis();         // Initialise millis count for seconds timing in loop()
} 
 
 
void loop() 
{

  // Check if millis has wrapped and we now have a useless value
  if(millis() < lastpoll) { 
    lastpoll = millis();  // Yes it has reload it
  } else {    
    // Otherwise if we are due another poll do it
    if (millis() - lastpoll >= TEMP_PERIOD) {
      lastpoll = millis();                // store the time this pulse took place      

      // Read the temperature
      get_temp();
  
    } 
  }    
  
  // Check if millis has wrapped and we now have a useless value
  if(millis() < lastpulse) { 
    lastpulse = millis();  // Yes it has reload it
  } else {    
    // Otherwise if we are due another poll do it
    if (millis() - lastpulse >= SERVO_PERIOD) {
      lastpulse = millis();                // store the time this pulse took place      

      // Refresh the servo position
      do_servo();
  
    } 
  }  
    
  // Once a second do something useful
  // If millis count has wraped around re read it, otherwise see if a second has elapsed
  if(millis() < last_second) { 
    last_second = millis();
  } else {  
    if (millis() - last_second >= ONE_SECOND) {
      // reload last_second to mark this cycle
      last_second = millis();
    
      toggle_debug(); // toggle the debug pin 
  
      // Warm the ducks butt
      butt_heat();    

      // Move the ducks butt
      butt_move();

      // Tell everyone how well the duck is doing
      report_status();
    }
  }
  
}


// toggles the debug pin
void toggle_debug() {
  
  // if the debug pin is off turn it on and vice-versa:
  if (debug_state == LOW)
    debug_state = HIGH;
  else
    debug_state = LOW;

  // set the debug pin with the debug state of the variable:
  digitalWrite(DEBUG_PIN, debug_state);
  
}


// Report the incubator status via the serial host link
void report_status() {
  
      // Report the status of the incubator
      if( ducksbutt_dir == FORWARD ) {
        sprintf(buff,"%d,FWD,%d,%d,%d,%d\n", ducksbutt_pos, ducksbutt_setpoint, ducksbutt_temp, PIDerror, pwr_level);
      } else {
        sprintf(buff,"%d,BWD,%d,%d,%d,%d\n", ducksbutt_pos, ducksbutt_setpoint, ducksbutt_temp, PIDerror, pwr_level);
      }
      Serial.print(buff);     
  
}


// Heats the ducks butt
void butt_heat() {
  
  // Calculate PID control values    
  PIDerror = ducksbutt_temp -  ducksbutt_setpoint;      // Calculate the error between actual and set point
  pwr_level = PIDerror * GAIN_PROP;                     // Set the pwr level using the proportional term
  PIDintgerror += PIDerror;                             // Integrate the current error with the running total
  pwr_level += PIDintgerror * GAIN_INTG;                // Sum the pwr level and integral term
//  pwr_level += (PIDlasterror - PIDerror) * GAIN_DIFF;   // Sum the pwr level and the differential term

  // Apply the new power level 
  if( pwr_level <= PWM_MAX && pwr_level >= PWM_MIN) {   //If resulting power level is in range for analogwrite use it
    analogWrite(BUTT_HEATER, pwr_level);                // Write new power level out
  } else {
    if( pwr_level < PWM_MIN ) {                         // If resulting power level is negative
      analogWrite(BUTT_HEATER, PWM_MIN);                // Write PWM_MIN out as new power level
    } else {                                            // Otherwise the value must be too big
      analogWrite(BUTT_HEATER, PWM_MAX);                // Write PWM_MAX out as new power level
    }
  }

  // cap the integrating error for next time around to prevent integral wind up
  if( PIDintgerror > CAPP_INTG ) {
    PIDintgerror = CAPP_INTG;  
  }
  
  if( PIDintgerror < CAPN_INTG ) {
    PIDintgerror = CAPN_INTG; 
  }
  
  // Tidy up loose ends store the error value for this round ready for the next round  
  PIDlasterror = PIDerror;                              // Save current error value for next time

}  
  
  
// Sweeps the ducks butt servo from MIN_SPULSE to MAX_SPULSE and back again
// Takes MAX_SPULSE - MIN_SPULSE seconds to complete a half sweep
void butt_move() {
  
      // FORWARDs if going FORWARDs, change direction if at end
      if( ducksbutt_dir == FORWARD ) {
        if( ducksbutt_pos == MAX_SPULSE ) {
          ducksbutt_dir = BACKWARD;
        } else {
          ducksbutt_pos++;
        }
      }
    
      // BACKWARDs if going BACKWARDs, change direction if at end
      if( ducksbutt_dir == BACKWARD ) {
        if( ducksbutt_pos == MIN_SPULSE ) {      
          ducksbutt_dir = FORWARD;
        } else {     
          ducksbutt_pos--;
        }
      }
    
}


// Call this and it will sort out the servo pulse generation
void do_servo(){

      digitalWrite(BUTT_POSITIONER, HIGH); // start the pulse  
      delayMicroseconds(ducksbutt_pos);    // wait for the pulse to be long enough  
      digitalWrite(BUTT_POSITIONER, LOW);  // stop the pulse
  
}  


// Call this and it will read the temp and average the last five polls
void get_temp(){

  // Read the temperature and do a simple running average to apply a little low pass filtering.
  // There is some electrical noise present from the heater drive circuitry
  //dbhv5 = dbhv4;
  //dbhv4 = dbhv3;
  //dbhv3 = dbhv2;
  //dbhv2 = dbhv1;
  //dbhv1 = analogRead(BUTT_TEMP);                        // Read the temperature of the ducks butt
  //ducksbutt_temp = (dbhv1 + dbhv2 + dbhv3 + dbhv4 + dbhv5) / 5; // Calcualte the average of the values
  ducksbutt_temp = analogRead(BUTT_TEMP);                        // Read the temperature of the ducks butt
  

}

