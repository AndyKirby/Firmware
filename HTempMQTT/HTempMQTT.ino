/*
  HTempMQTT
  Read temperature and humidity from dht22 then mesage it to MQTT Broker
*/
#include <DHT.h>
#include <PubSubClient.h>
#include <ESP8266WiFi.h>


#define DHTPIN 0          // DHT22 connected to gpio2
#define DHTTYPE DHT22     // Calls are to DHT22 type sensor not DHT11
#undef MQTT_KEEPALIVE     // redefine mqtt keepalive period in seconds, about 3 loops
#define MQTT_KEEPALIVE 60
#define Interval 20000UL  // Number of milli that each loop should take to complete

// Wifi and MQTT specifics
   // Wifi network credentials
   const char* wifissid = "MyWifiSSID";   // Wifi noetwork name or ESSID/SSID
   const char* wifipwd = "MyWififPassword";   // Wifi password

   // MQTT server credentials
   const char* nodeprefix = "THSensor-";   // Prefix for node name rest is mac address
   const char* mqttsrvr = "192.168.1.x";    // Ip address of mqtt broker
   const char* mqttuid = "MyMQTTBrokerUser";   // mqtt broker username
   const char* mqttpwd = "MyMQTTBrokerPassword"; // mqtt broker password

   // MQTT topics for the node
   const char* topic_node = "MyNoT/WhereEver/THSensor/node";   // topic name for LWT node up/down notifications
   const char* topic_temperature = "MyNoT/WhereEver/THSensor/sensor/temp";   // topic name for temperature readings
   const char* topic_humidity = "MyNoT/WhereEver/NoTLamp/THSensor/humid";   // topic name for humidity readings
   const char* topic_light = "MyNoT/WhereEver/NoTLamp/THSensor/light";   // topic name for light level readings

String NodeName="";

// tx pacing counter
int pacing = 0;

// variable that holds the terminal millis count for a loop cycle
unsigned long nextturn; 


// Initialize DHT sensor with threshold parameter of 15 as advised in https://github.com/esp8266/Arduino
DHT dht(DHTPIN, DHTTYPE, 15);

//Initialize mqtt and wifi
WiFiClient wifiClient;
PubSubClient client(mqttsrvr, 1883, callback, wifiClient);

// arrived message callback subroutine
void callback(char* topic, byte* payload, unsigned int length) {
   // handle message arrived
   return;
} 

// Convert 6 byte mac address to text string subroutine
String macToStr(const uint8_t* mac)
{
   String result;
   
   for (int i = 0; i < 6; ++i) {
      result += String(mac[i], 16);
   }
   
   return result;
}

// Connecto to Wifi subroutine
void WifiConnect() {
  int timout = 30;
  
  Serial.print("Wifi connecting to ");
  Serial.println(wifissid);
  WiFi.begin(wifissid, wifipwd);
  while (WiFi.status() != WL_CONNECTED) {
     delay(1000);
     Serial.print(".");
     if(--timout < 0){
        Serial.println("WIFI connect failed reset and try again...");
        abort();
     }       
  }
  Serial.println("");
  Serial.print("WiFi connected with IP ");
  Serial.println(WiFi.localIP()); 
  return;
}

// Connect to MQTT Broker subroutine
void BrokerConnect() {
   uint8_t mac[6];
   String lwtUp;
   String lwtDn; 
  
   if(NodeName.length() <= 0){
      // Generate client name and LWT messages based on MAC address and last 8 bits of microsecond counter
      WiFi.macAddress(mac);   
      NodeName = nodeprefix + macToStr(mac);
      lwtUp = NodeName + " is alive";
      lwtDn = NodeName + " is dead";
   } 
  
   Serial.print(NodeName);
   Serial.print(" connecting to ");
   Serial.print(mqttsrvr);
   Serial.print(" as ");
   Serial.println(mqttuid);

   if (client.connect((char*)NodeName.c_str(), mqttuid, mqttpwd, topic_node, 0, 0,  (char*)lwtDn.c_str())) {
      Serial.println("Connected to MQTT broker");
      Serial.print("LWT topic is: ");
      Serial.println(topic_node);
      if (client.publish(topic_node, (char*)lwtUp.c_str())) {
         Serial.println("Publish lwt up ok");
      } else {
         Serial.println("Publish lwt up failed");
      }
   } else {
      Serial.println("MQTT connect failed, reset and try again...");
      abort();
   }
  
   return; 
}  

// Humidity read and report subroutine.
void do_humidity(){
   float result;
   String humidity;   
  
  // humidity as %rh
   result = dht.readHumidity();
   if(!isnan(result)){
      humidity = result;
      if (client.publish(topic_humidity, (char*)humidity.c_str())) {
         Serial.print("Humidity: ");
         Serial.print(humidity);
         Serial.println("%");
      } else {
         Serial.println("Failed to publish humidity!");
      }
   } else {
     Serial.println("Failed to read humidity from DHT sensor!");
   }
   
   return;
}

// Temperature read and report subroutine
void do_temperature(){
   float result;
   String temperature;
   
   // temperature as centigrade
   result = dht.readTemperature();
   if(!isnan(result)){ 
      temperature = result;
      if (client.publish(topic_temperature, (char*)temperature.c_str())) {
         Serial.print("Temperature: ");
         Serial.print(temperature);
         Serial.println("*C");        
      } else {
         Serial.println("Failed to publish temperature!");
      }
   } else {
      Serial.println("Failed to read temperature from DHT sensor!");
   }

   return;  
}
   
// Light read and report subroutine
void do_light(){
   float result;
   String light;
     
   // light as lux
   result = 1.0;
   if(!isnan(result)){
      light = result;
      if(client.publish(topic_light, (char*)light.c_str())) {
         Serial.print("Light: ");
         Serial.print(light);
         Serial.println("Lx");        
      } else {
         Serial.println("Failed to publish light level!");
      }
   } else {
      Serial.println("Failed to read light level from light sensor!");
   }

   return;  
}

  
// Setuo routine runs once after reset  
void setup() {
   int startdelay;
   
   Serial.begin(115200);
   
   Serial.println("\n\nInitializing node");
  
   WifiConnect();
 
   BrokerConnect();   
 
   dht.begin();
   
   //seed the random number generator from micros
   randomSeed(micros());
   
   // put random delay in here between 0 and 20 seconds to lower probability
   // of all nodes talking at the same time after a power cut 
   startdelay = (int) random(0,21);
   
   Serial.print("Data collection starting in ");
   Serial.print(startdelay);
   Serial.println(" seconds");
   
   delay(startdelay * 1000);
   
   // Calculate when the first loops interval should finish ie
   // interval milliseconds from now
   nextturn = millis() + Interval;
   
   return;
  
}


// main loop runs forever
void loop() {
  
   // calculate when this time slot ends
   
  
   // Reconnectr to wifi if connection is lost
   if(WiFi.status() != WL_CONNECTED){
      WifiConnect();
   }
  
   // Reconnect to broker if conenction is lost
   if(!client.connected()) {
      BrokerConnect();
   }   

   // Do only one record and report per turn through the loop
   // to spread the broker and wifi loading by paceing the tx
   switch(pacing){
      case 0:
         do_humidity();
         pacing++;
         break;
      case 1:
         do_temperature();
         pacing++;
         break;
      case 2:
         do_light();
         pacing = 0;
         break;
   }
   
  // Waste the rest of this time slot
  // put power saving measures in this loop
  while( nextturn - millis() < Interval ) {
     delay(1);
  }
  
  // Calculate end of next cycle time  
  nextturn += Interval;

  return; 
}
